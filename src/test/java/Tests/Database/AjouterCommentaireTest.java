package Tests.Database;

import org.ecoleql.Pages.*;
import org.ecoleql.Tools.DatabaseConnection;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AjouterCommentaireTest {


    private WebDriver driver;
    private PageAccueil page_accueil;
    private PageLogin page_login;
    private PageMonCompte page_moncompte;
    private PageProduit page_produit;
    private DatabaseConnection database_connection;
    private Logger log;



    @BeforeEach
    public void init() {
        //instanciation du logger
        log = LoggerFactory.getLogger(this.getClass());
        //instanciation du driver
        System.setProperty ("webdriver.gecko.driver","src/main/resources/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        //accès à l'url de l'application
        driver.get("http://192.168.102.71:8081/");
        driver.manage().window().maximize();
        log.info("Test avec le navigateur Firefox sur Windows.");
    }

    @Test
    public void AjouterCommentaire() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        // instancier la page d'accueil du site
        page_accueil = PageFactory.initElements(driver, PageAccueil.class);
        log.info("Verification de la page d'accueil.");

        // Cliquer pour se connecter
        assertTrue(page_accueil.getElementConnexion().isEnabled());
        page_login = page_accueil.ClickSeConnecter(driver);

        // Se Connecter avec le User John Doe
        log.info("On se connecte avec l'utilisateur John Doe, Mail : pub@prestashop.com et Password : prestashop .");
        page_moncompte = page_login.SeConnecter(driver, "pub@prestashop.com", "prestashop");

        // On retourne sur la page d'Accueil
        log.info("On retourne sur sur la page d'accueil pour acceder à la rubrique populaire");
        page_accueil = page_moncompte.RetourHome(driver);

        // On clique sur le pull colibri des produits populaires
        log.info("On clique sur l'article pull colibri des produits populaires");
        page_produit = page_accueil.ClickArticlePopulairePullColibri(driver);

        // On clique sur ajouter un commentaire
        log.info("On clique pour ajouter un commentaire");
        page_produit.AjouterCommentaire(driver);
        assertTrue(page_produit.getTitreAjoutCommentaire().isEnabled());

        // On remplit les informations du commentaire
        log.info("On remplis le formulaire de commentaire");
        page_produit.CompleterCommentaire(driver);

        // Verification du pop up de validation du commentaire
        log.info("On vérifie la validation du pop up commentaire");
        assertTrue(page_produit.getTitrePopUpCommentaire().isEnabled());

        // Clique sur ok
        page_produit.cliqueOkCommentaire(driver);

        // Récupérer les informations nécessaires pour la vérification depuis la page ou ailleurs
        int productId = 2;
        int customerId = 2;
        String title = "Ceci est un titre de commentaire";
        String content = "Ceci est un avis pour un commentaire";
        float grade = 5.0f;

        // Vérification du commentaire dans la base de données
        log.info("On vérifie la presence du commentaire en base de données.");
        assertTrue(page_produit.verifyCommentInDatabase(productId, customerId, title, content, grade));

        // Modification et vérification de l'update de la colonne validate
        log.info("On modifie la validation du commentaire en base de données.");
        boolean isSuccess = page_produit.MajValidationCommentaire(driver, title, content);
        assertTrue(isSuccess);

        //Rafraichir la page
        log.info("On rafraichis la page pour actualiser l'apparition du commentaire");
        page_produit.rafraichirPage(driver);

        // Verification de la presence du commentaire
        log.info("On vérifie la presence du commentaire sur la page.");
        wait.until(ExpectedConditions.visibilityOf(page_produit.getVerifAjoutTitreCommentaire()));
        assertEquals(page_produit.getVerifAjoutTitreCommentaire().getText(), title);
        wait.until(ExpectedConditions.visibilityOf(page_produit.getVerifAjoutAvisCommentaire()));
        assertEquals(page_produit.getVerifAjoutAvisCommentaire().getText(), content );

        // Suppression du commentaire en base
        log.info("On supprime le commentaire en base de données.");
        page_produit.suppressionDuDernierCommentaire(driver, productId,customerId, title,content);

        //Rafraichir la page
        log.info("On rafraichis la page pour actualiser la suppression du commentaire");
        page_produit.rafraichirPage(driver);

        //Verification de la suppression
        assertTrue(wait.until(ExpectedConditions.invisibilityOf(page_produit.getVerifAjoutTitreCommentaire())));
        assertTrue(wait.until(ExpectedConditions.invisibilityOf(page_produit.getVerifAjoutAvisCommentaire())));
    }
    @AfterEach
    public void close()
    {
        driver.quit();
    }

}
