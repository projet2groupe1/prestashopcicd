package Tests.Database;

import org.ecoleql.Pages.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VerificationCommandeTest {
    private WebDriver driver;
    private PageAccueil page_accueil;
    private PageLogin page_login;
    private PageMonCompte page_moncompte;
    private PageProduit page_produit;
    private PageRecapProduit page_recapproduit;
    private PageInfosPersos page_infospersos;
    private PageRecapCommande page_recapcommande;
    private Logger log;


    @BeforeEach
    public void init() {
        //instanciation du logger
        log = LoggerFactory.getLogger(this.getClass());
        //instanciation du driver
        System.setProperty ("webdriver.gecko.driver","src/main/resources/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        //accès à l'url de l'application
        driver.get("http://192.168.102.71:8081/");
        driver.manage().window().maximize();
        log.info("Test avec le navigateur Firefox sur Windows.");
    }

    @Test
    public void VerificationCommande() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        // instancier la page d'accueil du site
        page_accueil = PageFactory.initElements(driver, PageAccueil.class);
        log.info("Verification de la page d'accueil.");

        // Cliquer pour se connecter
        log.info("On clique sur le bouton Connexion.");
        assertTrue(page_accueil.getElementConnexion().isEnabled());
        page_login = page_accueil.ClickSeConnecter(driver);

        // Se Connecter avec le User John Doe
        log.info("On se connecte avec l'utilisateur John Doe, Mail : pub@prestashop.com et Password : prestashop .");
        page_moncompte = page_login.SeConnecter(driver, "pub@prestashop.com", "prestashop");

        // On retourne sur la page d'Accueil
        log.info("On retourne sur sur la page d'accueil pour acceder à la rubrique populaire");
        page_accueil = page_moncompte.RetourHome(driver);

        // On clique sur l'article populaire tshirt imprime colibri
        log.info("On clique sur l'article populaire tshirt imprime colibri.");
        page_produit = page_accueil.ClickArticlePopulaireTshirtColibri(driver);

        // On vérifie qu'on est sur la bonne page de produit
        log.info("On vérifie la page du produit Tshirt Colibri");
        assertEquals(page_produit.getElementTitleProduit().getText(),"T-SHIRT IMPRIMÉ COLIBRI");

        // On vérifie le prix
        log.info("On vérifie le prix produit Tshirt Colibri");
        assertEquals(page_produit.getElementPrixProduit().getAttribute("content"),"22.94");

        // On clique sur ajouter au panier
        log.info("On clique sur le bouton 'Ajouter au panier'.");
        page_recapproduit = page_produit.CliqueAjoutPanier(driver);

        // On click sur continuer
        page_infospersos = page_recapproduit.Commander(driver);

        // On clique sur commander pour passer au paiement et on verifie la commande
        page_infospersos.ClickContinuerAdresse(driver);

        log.info("On vérifie la présence des du click and collect");
        assertTrue(page_infospersos.getElementClickAndCollect().isDisplayed());
        assertTrue(page_infospersos.getElementMyCarrier().isDisplayed());

        // On continuer vers la livraison
        page_infospersos.ClickContinuerLivraison(driver);
        assertTrue(page_infospersos.getElementCheckboxPaiementCarte().isDisplayed());

        // On continuer vers le paiement
        page_recapcommande = page_infospersos.Paiement(driver);
        wait.until(ExpectedConditions.visibilityOf(page_recapcommande.getElementCommandeConfirmee()));

        // On vérifie la commande à l'écran
        log.info("On vérifie la validation de la commande à l'écran");
        String title = page_recapcommande.getElementCommandeConfirmee().getText().substring(page_recapcommande.getElementCommandeConfirmee().getText().indexOf("VOTRE COMMANDE EST CONFIRMÉE"));
        assertEquals(title,"VOTRE COMMANDE EST CONFIRMÉE");
        assertEquals(page_recapcommande.getElementNbPanier().getText(),"(0)","Le nombre d'article dans le panier n'est pas celui attendu !!");

        // On vérifie la dernière commande en base de données
        log.info("On vérifie la presence de la commande en base de données.");
        page_recapcommande.effectuerVerificationCommande();

    }

    @AfterEach
    public void close()
    {
        driver.quit();
    }
}
