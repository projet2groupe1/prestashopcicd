package Tests.Compte;

import org.ecoleql.Pages.*;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

public class VerifDataPersoTest {

    private Logger log;

    private WebDriver driver;
    private PageAccueil page_accueil;
    private PageLogin  page_login;
    private PageMonCompte page_mon_Compte;
    private PageFormulaireInfosPersos page_formulaire_infos_persos;


    @BeforeEach
    public void init() {
        //instanciation du driver
        System.setProperty ("webdriver.gecko.driver","src/main/resources/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        //accès à l'url de l'application
        driver.get("http://192.168.102.71:8081/");
        driver.manage().window().maximize();
        //instanciation du logger
        log = LoggerFactory.getLogger(this.getClass());
        log.info("Test avec le navigateur Firefox sur Windows.");
    }

    @Test
    public void VerifierDataPerso() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        // Instancier la page d'accueil du site
        page_accueil = PageFactory.initElements(driver, PageAccueil.class);
        log.info("Instanciation de la page d'accueil du site");

        // V2 // Click pour se connecter via VOTRE COMPTE > Connexion
        page_login = page_accueil.ClickConnexionBasDePage(driver);
        log.info("Acces à la page Connectez-vous à votre compte");

        // Vérifications éléments formulaire Connexion
        assertTrue(page_login.getElementMdPOubli().isDisplayed(), "Lien 'Mot de passe oublie ?' absent !");
        assertTrue(page_login.getElementCreetionCompte().isDisplayed(), "Lien 'Pas de compte ? Creez-en un' absent");

        // Saisie des identifiants
        page_login.SaisieIdentifiants(driver, "pub@prestashop.com", "prestashop");

        // Vérification affichage champs E-mail et MdP
        assertEquals(page_login.getElementFieldEmail().getAttribute("value"), "pub@prestashop.com", "Pas le bon email !");
        assertEquals("password", page_login.getElementFieldMdp().getAttribute("type"), "le mot de passe est visible !");

        // V2 // Affichage du mot de passe
        page_login.AfficherMdP(driver);

        // Vérifcation affichage champ MdP
        assertEquals("text", page_login.getElementFieldMdp().getAttribute("type"), "le mot de passe n'est pas visible !");
        assertEquals(page_login.getElementFieldMdp().getAttribute("value"), "prestashop", "Pas le bon mot de passe");
        log.info("Vérifications de la page Connectez-vous à votre compte");


        // V2 // Accès à la page Mon compte
        page_mon_Compte = page_login.AccesMonCompte(driver);
        log.info("Acces à la page Mon compte");

        page_formulaire_infos_persos = page_mon_Compte.AccesFormulaireInfosPersos(driver);
        log.info("Acces à la page Vos informations personnelles");

        // V2 // Verifier Titre Vos informations personnelles
        wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains (text(),'Vos informations personnelles')]")));
        assertEquals(page_formulaire_infos_persos.getElementITitre().getText(), "Vos informations personnelles", "Ce n'est pas le bon titre H1 !");
        log.info("Wait page Vos informations personnelles");

        // Verifier Element Civilite M
        assertTrue(page_formulaire_infos_persos.getElementRadioM().isSelected(), "Ce n'est pas la bonne sélection !");

        // Verifier Element Prenom
        assertEquals(page_formulaire_infos_persos.getElementPrenom().getAttribute("value"), "John", "Ce n'est pas le bon prenom !");

        // Verifier Element Nom
        assertEquals(page_formulaire_infos_persos.getElementNom().getAttribute("value"), "DOE", "Ce n'est pas le bon nom !");

        // Verifier Element E-mail
        assertEquals(page_formulaire_infos_persos.getElementEmail().getAttribute("value"), "pub@prestashop.com", "Ce n'est pas le bon email !");

        // Verifier Element Mot de passe
        assertEquals(page_formulaire_infos_persos.getElementMdP().getAttribute("value"), "", "Le champ Mot de passe est rempli !");

        // Verifier Element Nouveau mot de passe
        assertEquals(page_formulaire_infos_persos.getElementNewMdP(driver).getAttribute("value"), "", "Le champ Nouveau mot de passe est rempli !");

        // Verifier Element Date de naissance
        assertEquals(page_formulaire_infos_persos.getElementDateNaissance().getAttribute("value"), "15/01/1970", "Ce n'est pas la bonne date");

        // Verifier Checkbox 'Recevoir les offres de nos partenaires'
        assertTrue(page_formulaire_infos_persos.getElementRecevoirOffres().isSelected(), "La checbox 'Recevoir les offres de nos partenaires' n'est pas cochee !");

        // Element Checkbox 'Message concernant la confidentialité des données clients'
        assertFalse(page_formulaire_infos_persos.getElementMessage().isSelected(), "La checbox 'Message concernant la confidentialité des données clients' est cochee !");

        // Verifier Checkbox 'Recevoir notre newsletter'
        assertTrue(page_formulaire_infos_persos.getElementRecevoirNewsletter().isSelected(), "La checbox 'Recevoir notre newsletter' n'est pas cochee !");

        // Element Checkbox 'J'accepte les conditions générales et la politique de confidentialité'
        assertFalse(page_formulaire_infos_persos.getElementConsentement().isSelected(), "La checbox 'J'accepte les conditions générales.. est cochee !");

        // Saisir les mots de passe
        page_formulaire_infos_persos.SaisieMotsDePasse(driver, page_formulaire_infos_persos.getElementMdP(), "prestashop");
        page_formulaire_infos_persos.SaisieMotsDePasse(driver, page_formulaire_infos_persos.getElementNewMdP(driver), "prestashop2");

        // Element Mot de passe
        assertEquals("password", page_formulaire_infos_persos.getElementMdP().getAttribute("type"), "le mot de passe est visible !");
        // Element Nouveau mot de passe
        assertEquals("password", page_formulaire_infos_persos.getElementNewMdP(driver).getAttribute("type"), "le nouveau mot de passe est visible !");

        // V2 // Afficher les mots de passe
        page_formulaire_infos_persos.MontrerMdP(driver, page_formulaire_infos_persos.getElementAfficherMdP());
        page_formulaire_infos_persos.MontrerMdP(driver, page_formulaire_infos_persos.getElementAfficherNewMdP());

        // Vérifcation de l'affichage des mots de passe
        wait.until(d -> {
            System.out.println(page_formulaire_infos_persos.getElementMdP().getAttribute("type"));
            return page_formulaire_infos_persos.getElementMdP().getAttribute("type").equals("text");
        });
        assertEquals("text", page_formulaire_infos_persos.getElementMdP().getAttribute("type"), "le mot de passe n'est pas visible !");
        assertEquals("text", page_formulaire_infos_persos.getElementNewMdP(driver).getAttribute("type"), "le nouveau mot de passe n'est pas visible !");
        log.info("Wait Affichage des mots de passe");

        // Cocher et décocher les checkboxes
        page_formulaire_infos_persos.CliqueCheckbox(driver, page_formulaire_infos_persos.getElementMessage());
        page_formulaire_infos_persos.CliqueCheckbox(driver, page_formulaire_infos_persos.getElementRecevoirNewsletter());
        page_formulaire_infos_persos.CliqueCheckbox(driver, page_formulaire_infos_persos.getElementConsentement());

        // Enregistrer les modifications
        page_formulaire_infos_persos.getElementEnregistrer().click();

        // Vérifier le message de confirmation
        assertEquals("Information mise à jour avec succès.", page_formulaire_infos_persos.getElementConfirmation().getText(), "Ce n'est pas le bon message ! Pas de message ?");

        // Checkbox 'Recevoir les offres de nos partenaires'
        assertTrue(page_formulaire_infos_persos.getElementRecevoirOffres().isSelected(), "La checkbox 'Recevoir les offres de nos partenaires' n'est pas cochee !");

        // Verifier Checkbox 'Recevoir notre newsletter'
        assertFalse(page_formulaire_infos_persos.getElementRecevoirNewsletter().isSelected(), "La checkbox 'Recevoir notre newsletter' n'est pas cochee !");
        log.info("Verification des modifications des informations personnelles");

        // Remise à l'état initial en fin de test
        page_formulaire_infos_persos.SaisieMotsDePasse(driver, page_formulaire_infos_persos.getElementMdP(), "prestashop2");
        page_formulaire_infos_persos.SaisieMotsDePasse(driver, page_formulaire_infos_persos.getElementNewMdP(driver), "prestashop");
        page_formulaire_infos_persos.CliqueCheckbox(driver, page_formulaire_infos_persos.getElementMessage());
        page_formulaire_infos_persos.CliqueCheckbox(driver, page_formulaire_infos_persos.getElementRecevoirNewsletter());
        page_formulaire_infos_persos.CliqueCheckbox(driver, page_formulaire_infos_persos.getElementConsentement());
        page_formulaire_infos_persos.CliqueCheckbox(driver, page_formulaire_infos_persos.getElementEnregistrer());
        log.info("Remise à l'etat initial");
    }

    @AfterEach
    public void close() {
        driver.quit();
    }
}
