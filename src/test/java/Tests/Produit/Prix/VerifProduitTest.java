package Tests.Produit.Prix;

import org.ecoleql.Pages.*;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VerifProduitTest {

    private WebDriver driver;
    private PageAccueil page_accueil;
    private PageAccessoires page_accessoires;
    private PageVetements page_vetements;
    private PageProduit page_produit;


    @BeforeEach
    public void init() {
        //instanciation du driver
        System.setProperty ("webdriver.gecko.driver","src/main/resources/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        //accès à l'url de l'application
        driver.get("http://192.168.102.71:8081/");
        driver.manage().window().maximize();
    }

    @Test
    public void VerifProduit() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        // instancier la page d'accueil du site
        page_accueil = PageFactory.initElements(driver, PageAccueil.class);
        assertTrue(page_accueil.getElementConnexion().isDisplayed());
        assertEquals(page_accueil.getElementNbPanier().getText(), "(0)", "Le nombre d'article dans le panier n'est pas celui attendu !!");
        assertTrue(page_accueil.getElementProduitColibri().isDisplayed());

        // Cliquer sur la categorie
        page_accessoires = page_accueil.ClickCategorie(driver,"accessoires");
        assertEquals(page_accessoires.getElementSousCategorie().getText(),"Sous-catégories");
        assertEquals(page_accessoires.getElementTItleFiltrerPar().getText(),"FILTRER PAR");
        assertEquals(page_accessoires.getElementCategorie().getText(),"ACCESSOIRES");
        assertTrue(page_accessoires.getElementProduitCarnetOurs().isDisplayed());

        int nbArticles = page_accessoires.Filtrer(driver,"Carton recyclé");
        assertEquals(nbArticles,3);
        page_produit = page_accessoires.ClickProduit(driver,"Carnet De Notes Ours Brun");
        assertEquals(page_produit.getElementTitleProduit().getText(),"CARNET DE NOTES OURS BRUN");
        assertEquals(page_produit.getElementPrixProduit().getText(),"15,48 €");
    }
    @AfterEach
    public void close()
    {
        driver.quit();
    }
}
