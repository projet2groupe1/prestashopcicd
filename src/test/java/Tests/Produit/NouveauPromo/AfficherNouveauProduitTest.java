package Tests.Produit.NouveauPromo;

import org.ecoleql.Pages.PageAccueil;
import org.ecoleql.Pages.PageLogin;
import org.ecoleql.Pages.PageNouveuxProduit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AfficherNouveauProduitTest {

    private WebDriver driver;
    private PageAccueil page_accueil;
    private PageNouveuxProduit page_nouveauxproduit;

    @BeforeEach
    public void init() {
        //instanciation du driver
        System.setProperty ("webdriver.gecko.driver","src/main/resources/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        //accès à l'url de l'application
        driver.get("http://192.168.102.71:8081/");
        driver.manage().window().maximize();
    }

    @Test
    public void AfficherNouveauProduit() {
        // instancier la page d'accueil du site
        page_accueil = PageFactory.initElements(driver, PageAccueil.class);
        assertTrue(page_accueil.getElementConnexion().isDisplayed());
        assertEquals(page_accueil.getElementNbPanier().getText(), "(0)", "Le nombre d'article dans le panier n'est pas celui attendu !!");
        assertTrue(page_accueil.getElementProduitColibri().isDisplayed());
        assertTrue(page_accueil.getElementCategorieNouveauProduit().isDisplayed());
        assertTrue(page_accueil.getElementNouveau().isDisplayed());
        page_nouveauxproduit = page_accueil.ClickTousNouveauxProduits(driver);
        assertTrue(page_nouveauxproduit.getElementProduitOurs().isDisplayed());
        assertTrue(page_nouveauxproduit.getElementSousCategorieVetements().isDisplayed());
        assertTrue(page_nouveauxproduit.getElementSousCategorieAccessoires().isDisplayed());
        assertTrue(page_nouveauxproduit.getElementSousCategorieArt().isDisplayed());

    }
    @AfterEach
    public void close()
    {
        driver.quit();
    }
}
