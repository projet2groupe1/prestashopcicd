package Tests.Produit.Recherche;

import org.ecoleql.Pages.PageAccueil;
import org.ecoleql.Pages.PageProduit;
import org.ecoleql.Pages.PageResultatRecherche;

import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RechercheProduitTest {

    private WebDriver driver;
    private PageAccueil page_accueil;
    private PageResultatRecherche page_resultatrecherche;
    private PageProduit page_produit;
    private Logger log;

    @BeforeEach
    public void init() {
        //instanciation du logger
        log = LoggerFactory.getLogger(this.getClass());
        //instanciation du driver
        System.setProperty("webdriver.gecko.driver","src/main/resources/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        //accès à l'url de l'application
        driver.get("http://192.168.102.71:8081/");
        driver.manage().window().maximize();
        log.info("Test avec le navigateur Firefox sur Windows.");
    }

    @Test
    public void RechercherProduit() {
        // instancier la page d'accueil du site
        page_accueil = PageFactory.initElements(driver, PageAccueil.class);
        assertTrue(page_accueil.getElementConnexion().isDisplayed());
        assertEquals(page_accueil.getElementNbPanier().getText(),"(0)","Le nombre d'article dans le panier n'est pas celui attendu !!");
        assertTrue(page_accueil.getElementProduitColibri().isDisplayed());
        log.info("Verification de la page d'accueil.");
        log.info("On entre 'Mug Today Is A Good Day' dans la barre de recherche .");
        page_resultatrecherche = page_accueil.Rechercher(driver,"Mug Today Is A Good Day");
        assertTrue(page_resultatrecherche.getElementProduit().isDisplayed());
        page_produit = page_resultatrecherche.ClickProduit(driver);
        assertEquals(page_produit.getElementPrixProduit().getAttribute("content"),"14.28");
        assertEquals(page_produit.getElementTitleProduit().getText(),"MUG TODAY IS A GOOD DAY");
    }

    @AfterEach
    public void close()
    {
        driver.quit();
    }
}

