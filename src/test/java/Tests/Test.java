package Tests;

import io.restassured.RestAssured.*;
import io.restassured.matcher.RestAssuredMatchers.*;
import org.hamcrest.Matchers.*;
import org.junit.jupiter.api.BeforeAll;

import static io.restassured.RestAssured.given;
import java.util.Base64;

public class Test {
    String apiKey = "LB4HFSA8MBC1Y8RA6H73FDEPWYZERHLR";
    String apiUrl = "192.168.102.49:8080";

    public void testing() {
        String authorizationKey = Base64.getEncoder().encodeToString((apiKey.getBytes()));
        given().headers("Output-Format", "JSON", "Authorization", authorizationKey).and().get(apiUrl);

    }
}
