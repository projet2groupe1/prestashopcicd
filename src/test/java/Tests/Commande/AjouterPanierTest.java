package Tests.Commande;
import org.ecoleql.Pages.*;

import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AjouterPanierTest {
    private WebDriver driver;
    private PageAccueil page_accueil;
    private PageLogin page_login;
    private PageMonCompte page_moncompte;
    private PageArt page_art;
    private PageProduit page_produit;
    private PageRecapProduit page_recapproduit;
    private PageInfosPersos page_infospersos;
    private PageRecapCommande page_recapcommande;
    private Logger log;
    @BeforeEach
    public void init() {
        //instanciation du logger
        log = LoggerFactory.getLogger(this.getClass());
        //instanciation du driver
        System.setProperty ("webdriver.gecko.driver","src/main/resources/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        //accès à l'url de l'application
        driver.get("http://192.168.102.71:8081/");
        driver.manage().window().maximize();
        log.info("Test avec le navigateur Firefox sur Windows.");
    }
    @Test
    public void AjouterPanier() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        // instancier la page d'accueil du site
        page_accueil = PageFactory.initElements(driver,PageAccueil.class);
        assertTrue(page_accueil.getElementConnexion().isDisplayed());
        assertEquals(page_accueil.getElementNbPanier().getText(),"(0)","Le nombre d'article dans le panier n'est pas celui attendu !!");
        assertTrue(page_accueil.getElementProduitColibri().isDisplayed());
        log.info("Verification de la page d'accueil.");
        // Cliquer pour se connecter
        log.info("On clique sur le bouton Connexion.");
        page_login = page_accueil.ClickSeConnecter(driver);
        // Se Connecter avec le User John Doe
        log.info("On se connect avec l'utilisateur John Doe, Mail : pub@prestashop.com et Password : prestashop .");
        page_moncompte = page_login.SeConnecter(driver, "pub@prestashop.com","prestashop");
        // Assert
        assertTrue(page_login.getElementUserJohnDoe().isDisplayed());
        assertTrue(page_accueil.getElementSeDeconnecter().isDisplayed());

        // On retourne sur la page d'Accueil
        page_accueil = page_moncompte.RetourHome(driver);
        // On click sur "Art"
        log.info("On clique sur la catégorie Art.");
        page_art = page_accueil.ClickCategorie(driver,"art");
        assertEquals(page_art.getElementTItleFiltrerPar().getText(),"FILTRER PAR");
        assertTrue(page_art.getElementImageArticle().isDisplayed());

        // On click sur le produit "AFFICHE ENCADRÉE THE BEST IS YET TO COME"
        log.info("On clique sur le produit \"AFFICHE ENCADRÉE THE BEST IS YET TO COME\".");
        page_produit = page_art.ClickProduit(driver);
        assertEquals(page_produit.getElementPrixProduit().getAttribute("content"),"34.8");
        assertEquals(page_produit.getElementTitleProduit().getText(),"AFFICHE ENCADRÉE THE BEST IS YET TO COME");

        // On choisit les articles que l'on veut commander
        page_produit.ChoisirArticle(driver,null);
        log.info("On clique sur le bouton 'Ajouter au panier'.");
        page_produit.AjoutPanier(driver);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        wait.until(ExpectedConditions.visibilityOf(page_produit.getElementTitleModal()));
        String title = page_produit.getElementTitleModal().getText().substring(page_produit.getElementTitleModal().getText().indexOf("Produit ajouté au panier avec succès"));
        assertEquals(title,"Produit ajouté au panier avec succès");
        assertEquals(page_produit.getElementTransportPrix().getText(),"gratuit");
        assertEquals(page_produit.getElementPrixTTC().getText(),"69,60 €");

        // On click sur continuer
        log.info("On clique sur le bouton 'Continuer mes achats'.");
        page_produit.clickCommanderOuContinuer(driver,true);
        wait.until(ExpectedConditions.visibilityOf(page_produit.getElementTitleProduit()));
        assertEquals(page_produit.getElementTitleProduit().getText(),"AFFICHE ENCADRÉE THE BEST IS YET TO COME");
        assertEquals(page_produit.getElementNbPanier().getText(),"(2)","Le nombre d'article dans le panier n'est pas celui attendu !!");

        log.info("On change la dimension de l'article.");
        page_produit.ChoisirArticle(driver,"2");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        assertEquals(page_produit.getElementPrixProduit().getAttribute("content"),"58.8");
        log.info("On clique sur le bouton 'Ajouter au panier'.");
        page_produit.AjoutPanier(driver);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        wait.until(ExpectedConditions.visibilityOf(page_produit.getElementTitleModal()));
        title = page_produit.getElementTitleModal().getText().substring(page_produit.getElementTitleModal().getText().indexOf("Produit ajouté au panier avec succès"));
        assertEquals(title,"Produit ajouté au panier avec succès");
        assertEquals(page_produit.getElementTransportPrix().getText(),"gratuit");
        assertEquals(page_produit.getElementPrixTTC().getText(),"128,40 €");
        // On click sur continuer
        log.info("On clique sur le bouton 'Continuer mes achats'.");
        page_produit.clickCommanderOuContinuer(driver,true);
        wait.until(ExpectedConditions.visibilityOf(page_produit.getElementTitleProduit()));
        assertEquals(page_produit.getElementTitleProduit().getText(),"AFFICHE ENCADRÉE THE BEST IS YET TO COME");
        assertEquals(page_produit.getElementNbPanier().getText(),"(3)","Le nombre d'article dans le panier n'est pas celui attendu !!");

        log.info("On change la dimension de l'article.");
        page_produit.ChoisirArticle(driver,"3");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        assertEquals(page_produit.getElementPrixProduit().getAttribute("content"),"94.8");

        log.info("On clique sur le bouton 'Ajouter au panier'.");
        page_produit.AjoutPanier(driver);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        wait.until(ExpectedConditions.visibilityOf(page_produit.getElementTitleModal()));
        title = page_produit.getElementTitleModal().getText().substring(page_produit.getElementTitleModal().getText().indexOf("Produit ajouté au panier avec succès"));
        assertEquals(title,"Produit ajouté au panier avec succès");
        assertEquals(page_produit.getElementTransportPrix().getText(),"gratuit");
        assertEquals(page_produit.getElementPrixTTC().getText(),"223,20 €");

        log.info("On clique sur le bouton 'Commander'.");
        page_recapproduit = page_produit.clickCommanderOuContinuer(driver,false);

        // On supprimer le dernier article
        log.info("On Supprime l'article cadre 80x120cm.");
        page_recapproduit.Supprimer(driver);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        wait.until(ExpectedConditions.invisibilityOf(page_recapproduit.getElementProduit80x120()));
        assertEquals(page_recapproduit.getElementTotalPanier().getText(),"128,40 €");
        assertEquals(page_produit.getElementNbPanier().getText(),"(3)","Le nombre d'article dans le panier n'est pas celui attendu !!");

        // On click sur commander pour passer au paiment
        log.info("On clique sur le bouton 'Commander' pour passer au recap des infos persos, livraison et paiement.");
        page_infospersos = page_recapproduit.Commander(driver);
        assertTrue(page_infospersos.getElementSectionInfoPerso().isDisplayed());
        page_infospersos.ClickContinuerAdresse(driver);
        assertTrue(page_infospersos.getElementClickAndCollect().isDisplayed());
        assertTrue(page_infospersos.getElementMyCarrier().isDisplayed());
        page_infospersos.ClickContinuerLivraison(driver);
        assertTrue(page_infospersos.getElementCheckboxPaiementCarte().isDisplayed());
        page_recapcommande = page_infospersos.Paiement(driver);
        wait.until(ExpectedConditions.visibilityOf(page_recapcommande.getElementCommandeConfirmee()));
        title = page_recapcommande.getElementCommandeConfirmee().getText().substring(page_recapcommande.getElementCommandeConfirmee().getText().indexOf("VOTRE COMMANDE EST CONFIRMÉE"));
        assertEquals(title,"VOTRE COMMANDE EST CONFIRMÉE");
        assertEquals(page_recapcommande.getElementPrixUnitaireArticle1().getText(),"34,80 €");
        assertEquals(page_recapcommande.getElementPrixUnitaireArticle2().getText(),"58,80 €");

        assertEquals(page_recapcommande.getElementPrixTotalArticle1().getText(),"69,60 €");
        assertEquals(page_recapcommande.getElementPrixTotalArticle2().getText(),"58,80 €");
        assertEquals(page_recapcommande.getElementPrixTotalTTC().getText(),"128,40 €");
        assertEquals(page_recapcommande.getElementNbPanier().getText(),"(0)","Le nombre d'article dans le panier n'est pas celui attendu !!");
        log.info("Commande effectuée et vérifications faites.");
    }

    @AfterEach
    public void close()
    {
        //driver.quit();
    }
}

