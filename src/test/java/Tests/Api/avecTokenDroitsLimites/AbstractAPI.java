package Tests.Api.avecTokenDroitsLimites;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.ecoleql.Tools.OutilsApi;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;

public class AbstractAPI {
    static final String apiKey = "AB65Q1LXFKZB3ECSRITM7LC4RW59ZN74"; // droits limités
    static final String apiUrl = "http://192.168.102.71/api";
    static final Integer apiPort = 8081;
    protected static RequestSpecification headersSpec;
    Logger log;
    OutilsApi outilsApi;

    @BeforeEach
    public void apiInit() {
        log = LoggerFactory.getLogger(this.getClass());
        log.info("Test débuté - avecTokenDroitsLimites.");
        log.info("Prérequis en cours de formation.");
        RestAssured.baseURI = apiUrl;
        RestAssured.port = apiPort;

        // Conversion en Base64 du token
        String tempApiKey = apiKey + ":";
        String finalKey = Base64.getEncoder().encodeToString((tempApiKey.getBytes()));
        headersSpec = RestAssured.given()
                .header("Authorization", "Basic " + finalKey);
//                .header("Output-Format", "JSON");
        outilsApi = new OutilsApi(headersSpec);
        log.info("Prérequis OK - Avoir un access token sans les droits sur l'api products.");
    }
    @AfterEach
    public void apiEnd() {
        log.info("Test terminé.");
    }
}
