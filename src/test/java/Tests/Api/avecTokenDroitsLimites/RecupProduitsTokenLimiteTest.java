package Tests.Api.avecTokenDroitsLimites;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.XML;
import static org.junit.jupiter.api.Assertions.fail;

public class RecupProduitsTokenLimiteTest extends AbstractAPI {
    @Test
    public void RecupProduitsTokenLimite() {
        log.info("Step 1 - Début : En utilisant l'api products, rechercher la liste des produits avec un access token sans les droits pour l'api.");

        Response apiResponseStep1 = given().spec(headersSpec)
                .when()
                .get("/products")
                .then()
                .contentType(XML)
                .extract()
                .response();

//        System.out.println(apiResponseStep1.getBody().asString()); // debug : imprime le log

        int checkStatusCode = apiResponseStep1.getStatusCode();
        if (checkStatusCode == 401) {
            log.info("Passed. checkStatusCode = 401.");
            outilsApi.ApiIsEqual(apiResponseStep1, "prestashop.errors.error.message", "Resource of type \"products\" is not allowed with this authentication key");
        } else {
            log.error("checkStatusCode != 401. Le code est : " + checkStatusCode);
            fail("checkStatusCode != 401. Le code est : " + checkStatusCode);
        }

        log.info("Step 1 - Fin : L'api retourne un code 401. La réponse xml comporte le message 'Resource of type \"products\" is not allowed with this authentication key'");
    }
}
