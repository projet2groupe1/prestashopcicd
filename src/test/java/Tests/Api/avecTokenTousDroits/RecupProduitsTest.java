package Tests.Api.avecTokenTousDroits;

import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import java.util.*;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.XML;
import static org.junit.jupiter.api.Assertions.fail;

public class RecupProduitsTest extends AbstractAPI {
    @Test
    void RecupProduits() {
        log.info("Step 1 - Début : En utilisant l'api products, rechercher le prix du produit ${produit}");

        List<JDD> testJDD = new ArrayList<>();
        testJDD.add(new JDD(3, "Affiche encadrée The best is yet to come", 29.00));
        testJDD.add(new JDD(13, "Illustration vectorielle Ours brun", 9.00));
        testJDD.add(new JDD(2, "Pull imprimé colibri", 35.90));
        testJDD.add(new JDD(8, "Mug Today is a good day", 11.90));

        String productNameErrorMsg = "";
        for (JDD test : testJDD) {
            Response apiResponse = outilsApi.ApiGetIdContent("products", test.productId);
            String xmlPathProductNames = "prestashop.product.name.language";
            String xmlPathProductPrice = "prestashop.product.price";
            List<String> productNames = apiResponse.xmlPath().getList(xmlPathProductNames);
            Double productPrice = apiResponse.xmlPath().getDouble(xmlPathProductPrice);
            boolean areNamesEqual = false;

            for (String pn : productNames) {
                productNameErrorMsg = pn;
                if (test.productName.equals(pn)) {
                    areNamesEqual = true;
                    log.info("apiResponse. Passed. JDD id (" + test.productId + ") : productName (" + test.productName + ") = api productName (" + pn + ") for xmlPath (" + xmlPathProductNames + ").");
                }
            }
            if (!areNamesEqual) {
                log.error("apiResponse. Failed. JDD id (" + test.productId + ") : productName (" + test.productName + ") != api productName (" + productNameErrorMsg + ") for xmlPath (" + xmlPathProductNames + ").");
                fail("apiResponse. Failed. JDD id (" + test.productId + ") : productName (" + test.productName + ") != api productName (" + productNameErrorMsg + ") for xmlPath (" + xmlPathProductNames + ").");
            }
            if (test.productPrice == productPrice) {
                log.info("apiResponse. Passed. JDD id (" + test.productId + ") : productPrice (" + test.productPrice + ") = api productPrice (" + productPrice + ") for xmlPath (" + xmlPathProductPrice + ").");
            } else {
                log.error("apiResponse. Failed. JDD id (" + test.productId + ") : productPrice (" + test.productPrice + ") != api productPrice (" + productPrice + ") for xmlPath (" + xmlPathProductPrice + ").");
                fail("apiResponse. Failed. JDD id (" + test.productId + ") : productPrice (" + test.productPrice + ") != api productPrice (" + productPrice + ") for xmlPath (" + xmlPathProductPrice + ").");
            }
        }

        log.info("Step 1 - Fin : L'api retourne un code 200. La réponse xml comporte bien le prix du produit avec la valeur ${prix}.");
    }
}

