package Tests.Api.avecTokenTousDroits;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.ecoleql.Pages.PageAccueil;
import org.ecoleql.Pages.PageFormulaireInfosPersos;
import org.ecoleql.Pages.PageLogin;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.opentest4j.AssertionFailedError;

import java.time.Duration;
import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class AjouterClientPassantTest extends AbstractAPI {
    private WebDriver driver;
    private PageAccueil page_accueil;
    private PageLogin page_login;
    private PageFormulaireInfosPersos page_formulaire_infos_persos;
    String urlBrowser = "http://192.168.102.71:8081/";

    @Test
    public void AjouterClientPassant() {
        log.info("Step 1 - Début : Vérifier qu'il n'y a pas de client ${firstname} ${lastname} avec l'api customers");

        Response apiResponseStep1 = outilsApi.ApiGetIdContent("customers", 1);

        int statusCode = apiResponseStep1.statusCode();
        if (statusCode == 200) {
            log.info("Passed. statusCode = 200.");
            outilsApi.ApiNotEqual(apiResponseStep1, "prestashop.customer.firstname", "cust");
            outilsApi.ApiNotEqual(apiResponseStep1, "prestashop.customer.lastname", "omer");
        } else {
            log.error("Échec de l'appel de l'API. Le status de la requête n'est pas 200, mais : " + statusCode);
            fail("Échec de l'appel de l'API. Le status de la requête n'est pas 200, mais : " + statusCode);
        }

        log.info("Step 1 - Fin : Résultat attendu : Le client ${firstname} ${lastname} n'est pas présent");


        log.info("Step 2 - Début : Créer le client : ${firstname} ${lastname} ${email} ${mdp}");

        String createClient = "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">" +
                "<customer>" +
//                "<id_default_group><![CDATA[3]]></id_default_group>" +
                "<firstname><![CDATA[cust]]></firstname>" +
                "<lastname><![CDATA[omer]]></lastname>" +
                "<email><![CDATA[customer@customer.com]]></email>" +
                "<passwd><![CDATA[customer]]></passwd>" +
                "<associations><groups><group><id><![CDATA[3]]></id></group></groups></associations>" + // Attention, obligatoire sinon pas d'authentification IHM
                "</customer>" +
                "</prestashop>";

        Response apiResponseStep2 = given()
                .spec(headersSpec)
                .header("Content-Type", "application/xml")
                .body(createClient)
                .when()
                .post("/customers");

//        System.out.println(apiResponseStep2.getBody().asString()); // debug : imprime l'xml

        int createClientStatusCode = apiResponseStep2.statusCode();
        if (createClientStatusCode == 201) {
            log.info("Passed. createClientStatusCode = 201.");
            outilsApi.ApiIsEqual(apiResponseStep2, "prestashop.customer.firstname", "cust");
            outilsApi.ApiIsEqual(apiResponseStep2, "prestashop.customer.lastname", "omer");
            outilsApi.ApiIsEqual(apiResponseStep2, "prestashop.customer.email", "customer@customer.com");
            outilsApi.ApiNotEqual(apiResponseStep2, "prestashop.customer.passwd", "customer");
        } else {
            log.error("createClientStatusCode != 201. Le code est : " + createClientStatusCode);
            fail("createClientStatusCode != 201. Le code est : " + createClientStatusCode);
        }

        String newClientId = apiResponseStep2.xmlPath().getString("prestashop.customer.id");
        log.info("Info : newClientId = " + newClientId); // debug : imprime l'id

        log.info("Step 2 - Fin : L'api renvoie bien un code 201. Le xml de retour comprend : les éléments renseignés, une id généré dynamiquement, mot de passe encrypté");



        log.info("Step 3 - Début : Rechercher le client créé et vérifier que le compte n'est pas actif");

        Response apiResponseStep3 = outilsApi.ApiGetIdContent("customers", Integer.valueOf(newClientId));
        int checkCreatedClientStatusCode = apiResponseStep3.statusCode();
        if (checkCreatedClientStatusCode == 200) {
            log.info("Passed. checkCreatedClientStatusCode = 200.");
            outilsApi.ApiIsEqual(apiResponseStep3, "prestashop.customer.active", "0");
            outilsApi.ApiIsEqual(apiResponseStep3, "prestashop.customer.firstname", "cust");
            outilsApi.ApiIsEqual(apiResponseStep3, "prestashop.customer.lastname", "omer");
            outilsApi.ApiIsEqual(apiResponseStep3, "prestashop.customer.email", "customer@customer.com");
            outilsApi.ApiNotEqual(apiResponseStep3, "prestashop.customer.passwd", "customer");
        } else {
            log.error("createClientStatusCode != 200. Le code est : " + createClientStatusCode);
            fail("createClientStatusCode != 200. Le code est : " + createClientStatusCode);
        }

        log.info("Step 3 - Fin : L'api renvoie un code 200 avec une réponse xml comprenant les éléments précédents avec le noeud active égale à 1");


        log.info("Step 4 - Début : Changer la valeur 'active' à 1 avec la méthode PATCH");

        String patchClient = "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">" +
                "<customer>" +
                "<id><![CDATA[" + newClientId + "]]></id>" +
                "<active><![CDATA[1]]></active>" +
                "</customer>" +
                "</prestashop>";

        Response apiResponseStep4 = given().spec(headersSpec)
                .header("Content-Type", "application/xml")
                .body(patchClient)
                .when()
                .patch("/customers/" + newClientId);

//        System.out.println(apiResponseStep4.getBody().asString()); // debug : imprime le log

        int patchClientStatusCode = apiResponseStep4.statusCode();
        if (patchClientStatusCode == 200) {
            log.info("Passed. patchClientStatusCode = 200.");
            outilsApi.ApiIsEqual(apiResponseStep4, "prestashop.customer.active", "1");
            outilsApi.ApiIsEqual(apiResponseStep4, "prestashop.customer.firstname", "cust");
            outilsApi.ApiIsEqual(apiResponseStep4, "prestashop.customer.lastname", "omer");
            outilsApi.ApiIsEqual(apiResponseStep4, "prestashop.customer.email", "customer@customer.com");
            outilsApi.ApiNotEqual(apiResponseStep4, "prestashop.customer.passwd", "customer");
        } else {
            log.error("patchClientStatusCode != 200. Le code est : " + patchClientStatusCode);
            fail("patchClientStatusCode != 200. Le code est : " + patchClientStatusCode);
        }

        log.info("Step 4 - Fin : L'api renvoie un code 200 avec une réponse xml comprenant les éléments précédents avec le noeud active égale à 1");



        log.info("Step 5 - Début : Se connecter à l'application prestashop et se connecter avec le client créé");

        //instanciation du driver
        System.setProperty ("webdriver.gecko.driver","src/main/resources/driver/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        //accès à l'url de l'application
        driver.get(urlBrowser);
        driver.manage().window().maximize();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        page_accueil = PageFactory.initElements(driver , PageAccueil.class);
        page_login = page_accueil.ClickSeConnecter(driver);
        page_accueil = page_login.Connexion(driver,"customer@customer.com","customer");
        assertTrue(page_accueil.getElementSeDeconnecter().isDisplayed());

        log.info("Step 5 - Fin : La page du compte s'affiche");



        log.info("Step 6 - Début : Se déconnecter");

        page_accueil.SeDeconnecter(driver);
        assertTrue(page_accueil.getElementConnexion().isDisplayed());
        driver.quit();

        log.info("Step 6 - Fin : La page d'accueil s'affiche avec la mention connexion en haut à droite");



        log.info("Step 7 - Début : Supprimer le client créé");

        Response apiResponseStep7 = given().spec(headersSpec)
                .header("Content-Type", "application/xml")
                .delete("/customers/" + newClientId);

        int checkDeleteStatusCode = apiResponseStep7.statusCode();
        if (checkDeleteStatusCode == 200) {
            log.info("Passed. checkDeleteStatusCode = 200.");
        } else {
            log.error("checkDeleteStatusCode != 200. Le code est : " + checkDeleteStatusCode);
            fail("checkDeleteStatusCode != 200. Le code est : " + checkDeleteStatusCode);
        }

        log.info("Step 7 - Fin : L'api renvoie un code 200");


        log.info("Step 8 - Début : Rechercher les clients disponibles");

        Response apiResponseStep8 = given().spec(headersSpec).
                when().
                get("/customers").
                then().
                contentType(ContentType.XML).
                extract().
                response();

//        System.out.println(apiResponseStep8.getBody().asString()); // debug : imprime le log

        try {
            outilsApi.ApiDoesElementsExist(apiResponseStep8, "prestashop.customers.customer.@id", Arrays.asList("1", "2", newClientId));
        } catch (AssertionFailedError error) {
            log.info("ApiDoesElementsExist. Passed. newClientId (" + newClientId + ") is absent.");
        }

        log.info("Step 8 - Fin : Le client supprimé n'est pas présent dans le retour de l'api");



        log.info("Step 9 - Début : Sur prestashop, essayer de se connecter avec le client supprimé");

        driver = new FirefoxDriver();
        driver.get(urlBrowser);
        driver.manage().window().maximize();

        page_accueil = PageFactory.initElements(driver , PageAccueil.class);
        page_login = page_accueil.ClickSeConnecter(driver);
        page_accueil = page_login.Connexion(driver,"customer@customer.com","customer");
        assertEquals(page_login.getElementEchecAuthentification().getText(), "Échec d'authentification");
        driver.quit();

        log.info("Step 9 - Fin : Un message d'erreur s'affiche et il correspond bien à 'Échec d'authentification'");
    }
}