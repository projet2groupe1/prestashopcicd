package Tests.Api.avecTokenTousDroits;

public class JDD {

    double productPrice;
    int productId;
    String productName;

    public JDD() {

    }

    public JDD(int productId, String productName, double productPrice) {
        this.productPrice = productPrice;
        this.productId = productId;
        this.productName = productName;
    }

    public double getproductPrice() {
        return productPrice;
    }

    public void setproductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getproductId() {
        return productId;
    }

    public void setproductId(int productId) {
        this.productId = productId;
    }

    public String getproductName() {
        return productName;
    }

    public void setproductName(String productName) {
        this.productName = productName;
    }
}
