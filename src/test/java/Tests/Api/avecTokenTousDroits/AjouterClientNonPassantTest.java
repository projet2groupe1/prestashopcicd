package Tests.Api.avecTokenTousDroits;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.*;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.XML;
import static org.junit.jupiter.api.Assertions.fail;

public class AjouterClientNonPassantTest extends AbstractAPI {
    @Test
    public void AjouterClientNonPassant() {
        log.info("Step 1 - Début : Rechercher la liste des clients avec l'api customer");
        Response apiResponseStep1 = given().spec(headersSpec)
                .when()
                .get("/customers")
                .then()
                .contentType(XML)
                .extract()
                .response();

//        System.out.println(apiResponseStep1.getBody().asString()); // debug : imprime le log

        int getClientsStatusCode = apiResponseStep1.getStatusCode();

        if (getClientsStatusCode == 200) {
            log.info("Passed. getClientsStatusCode = 200.");
            outilsApi.ApiDoesElementsExist(apiResponseStep1, "prestashop.customers.customer.@id", Arrays.asList("1", "2"));
        } else {
            log.error("getClientsStatusCode != 200. Le code est : " + getClientsStatusCode);
            fail("getClientsStatusCode != 200. Le code est : " + getClientsStatusCode);
        }

        log.info("Step 1 - Fin : L'api renvoie un code 200. Les clients id égale à 1 et 2 sont présent dans la réponse xml.");



        log.info("Step 2 - Début : Rechercher les informations du firstname, lastname, email et password du client avec l'id égale à 1");

        Response apiResponseStep2 = outilsApi.ApiGetIdContent("customers", 1);

//        System.out.println(apiResponseStep2.getBody().asString()); // debug : imprime le log

        String clientFirstName = apiResponseStep2.getBody().xmlPath().getString("prestashop.customer.firstname");
        String clientLastName = apiResponseStep2.getBody().xmlPath().getString("prestashop.customer.lastname");
        String clientEmail = apiResponseStep2.getBody().xmlPath().getString("prestashop.customer.email");
        String clientPasswd = apiResponseStep2.getBody().xmlPath().getString("prestashop.customer.passwd");

        int getClientStatusCode = apiResponseStep2.getStatusCode();
        if (getClientStatusCode == 200) {
            log.info("Passed. getClientStatusCode = 200.");

            Map<String, String> clientData = new HashMap<>();
            clientData.put("clientFirstName", clientFirstName);
            clientData.put("clientLastName", clientLastName);
            clientData.put("clientEmail", clientEmail);
            clientData.put("clientPasswd", clientPasswd);
            for (Map.Entry<String, String> checkData : clientData.entrySet()) {
                if (checkData.getValue().isEmpty()) {
                    log.error("apiResponseStep2. Failed. checkString ("+ checkData.getKey() + ") is empty.");
                    fail("apiResponseStep2. Failed. checkString ("+ checkData.getKey() + ") is empty.");
                }
            }
            log.info("apiResponseStep2. Passed. Les données du client sont valorisées.");
        } else {
            log.error("getClientStatusCode != 200. Le code est : " + getClientStatusCode);
            fail("getClientStatusCode != 200. Le code est : " + getClientStatusCode);
        }

        log.info("Step 2 - Fin : Résultat attentu : L'api renvoie un code 200. La réponse xml comporte bien ces informations");



        log.info("Step 3 - Début : Créer un client avec le même firstname, lastname, email et un mot de passe");

        String createClient = "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">" +
                "<customer>" +
                "<firstname><![CDATA[" + clientFirstName + "]]></firstname>" +
                "<lastname><![CDATA[" + clientLastName + "]]></lastname>" +
                "<email><![CDATA[" + clientEmail + "]]></email>" +
                "<passwd><![CDATA[ImANaughtyBoy]]></passwd>" +
                "</customer>" +
                "</prestashop>";

        Response apiResponseStep3 = given().spec(headersSpec)
                        .header("Content-Type", "application/xml")
                        .body(createClient)
                        .when()
                        .post("/customers");

//        System.out.println(apiResponseStep3.getBody().asString()); // debug : imprime le log

        int getCreateClientStatusCode = apiResponseStep3.getStatusCode();
        if (getCreateClientStatusCode == 500) {
            log.info("Passed. getCreateClientStatusCode = 500.");

            String mandatoryErrorToFind = "Cet e-mail est déjà utilisé, veuillez en choisir un autre";
            List<String> xmlErrorMessage = apiResponseStep3.xmlPath().getList("prestashop.errors.error.message"); // ce test vérifie implicitement si des messages d'erreurs existent
            if (xmlErrorMessage.contains(mandatoryErrorToFind)) {
                log.info("apiResponseStep3. Passed. Le message d'erreur suivant est présent : " + mandatoryErrorToFind);
            } else {
                log.error("apiResponseStep3. Failed. Le message d'erreur mandatoryErrorToFind (" + mandatoryErrorToFind + ") != mandatoryErrorFromXML (" + Arrays.toString(xmlErrorMessage.toArray()) + ").");
                fail("apiResponseStep3. Failed. Le message d'erreur mandatoryErrorToFind (" + mandatoryErrorToFind + ") != mandatoryErrorFromXML (" + Arrays.toString(xmlErrorMessage.toArray()) + ")."); // ça c'est beau
            }
        } else {
            log.error("getCreateClientStatusCode != 500. Le code est : " + getCreateClientStatusCode);
            fail("getCreateClientStatusCode != 500. Le code est : " + getCreateClientStatusCode);
        }

        log.info("Step 3 - Fin : L'api renvoie un code 500. La réponse xml comporte des erreurs avec le message 'Cet e-mail est déjà utilisé, veuillez en choisir un autre'");
    }
}