package Tests.Api.sansToken;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.XML;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class RecupProduitSansTokenTest extends AbstractAPI {
    @Test
    public void RecupProduitSansToken() {
        log.info("Step 1 - Début : En utilisant l'api products, rechercher la liste des produits sans access token");

        Response apiResponseStep1 = given().spec(headersSpec)
                .when()
                .get("/products")
                .then()
                .contentType("text/html; charset=utf-8")
                .extract()
                .response();

//        System.out.println(apiResponseStep1.getBody().asString()); // debug imprime le résultat

        int checkStatusCode = apiResponseStep1.getStatusCode();
        if (checkStatusCode == 401) {
            log.info("checkStatusCode. Passed. checkStatusCode = 401.");
            assertEquals("401 Unauthorized", apiResponseStep1.htmlPath().getString("html.body"));
            log.info("Passed. Le body contient l'erreur (401 Unauthorized)");
        } else {
            log.error("checkStatusCode. Failed. checkStatusCode != 401.");
            fail("checkStatusCode. Failed. checkStatusCode != 401.");
        }

        log.info("Step 1 - Fin : L'api retourne un code 401 avec seulement le message '401 Unauthorized'.");
    }
}
