package Tests.Api.sansToken;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.ecoleql.Tools.OutilsApi;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractAPI {
    static final String apiUrl = "http://192.168.102.71/api";
    static final Integer apiPort = 8081;
    protected static RequestSpecification headersSpec;
    Logger log;
    OutilsApi outilsApi;

    @BeforeEach
    public void apiInit() {
        log = LoggerFactory.getLogger(this.getClass());

        log.info("Test débuté - sansToken.");
        RestAssured.baseURI = apiUrl;
        RestAssured.port = apiPort;

        headersSpec = RestAssured.given();
        outilsApi = new OutilsApi(headersSpec);
        log.info("Prérequis OK.");
    }
    @AfterEach
    public void apiEnd() {
        log.info("Test terminé.");
    }
}
