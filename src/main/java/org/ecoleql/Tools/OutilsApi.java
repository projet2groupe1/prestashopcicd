package org.ecoleql.Tools;



import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;


public class OutilsApi {
    RequestSpecification headersSpec;
    Logger log;

    public OutilsApi(RequestSpecification headersSpec) {
        this.headersSpec = headersSpec;
    }
    public Response ApiGetIdContent(String function, Integer id) {
        return RestAssured.given().spec(headersSpec).
                when().
                get("/" + function + "/" + id).
                then().
                contentType(ContentType.XML).
                extract().
                response();
    }
    public Response ApiGetProductPrice(String function, String productName) {
        return RestAssured.given().spec(headersSpec).
                when().
                get("/" + function + "/" + productName).
                then().
                contentType(ContentType.XML).
                extract().
                response();
    }
    public Response ApiGetProductPriceAGAIN(String function, String productName) {
        return RestAssured.given().spec(headersSpec).
                log().
                all().
                when().
                get("/" + function + "/prestashop.product.name.language.@id" + productName).
                then().
                contentType(ContentType.XML).
                log().
                body().
                extract().
                response();
    }
    public void ApiIsEqual(Response response, String xmlPath, String valueShouldBe) {
        log = LoggerFactory.getLogger(this.getClass());
        String valueOfKey = response.xmlPath().getString(xmlPath);
        if (valueShouldBe.equals(valueOfKey)) {
            log.info("ApiIsEqual. Passed. valueShouldBe (" + valueShouldBe + ") = valueOfKey (" + valueOfKey + ") of xmlPath (" + xmlPath + ").");
        } else {
            log.error("ApiIsEqual. Fail. valueOfKey (" + valueOfKey + ") != valueShouldBe (" + valueShouldBe + ") of xmlPath (" + xmlPath + ").");
            fail("ApiIsEqual. Fail. valueOfKey (" + valueOfKey + ") != valueShouldBe (" + valueShouldBe + ") of xmlPath (" + xmlPath + ").");
        }
    }
    public void ApiNotEqual(Response response, String xmlPath, String valueShouldBe) {
        log = LoggerFactory.getLogger(this.getClass());
        String valueOfKey = response.xmlPath().getString(xmlPath);
        if (!valueShouldBe.equals(valueOfKey)) {
            log.info("ApiNotEqual. Passed. valueShouldBe (" + valueShouldBe + ") != valueOfKey (" + valueOfKey + ") of xmlPath (" + xmlPath + ").");
        } else {
            log.error("ApiNotEqual. Fail. valueOfKey (" + valueOfKey + ") = valueShouldBe (" + valueShouldBe + ") of xmlPath (" + xmlPath + ").");
            fail("ApiNotEqual. Fail. valueOfKey (" + valueOfKey + ") = valueShouldBe (" + valueShouldBe + ") of xmlPath (" + xmlPath + ").");
        }
    }
    public void ApiDoesElementsExist(Response response, String xmlPath, List<String> idsShouldBePresent) {
        log = LoggerFactory.getLogger(this.getClass());
        List<String> ids = response.xmlPath().getList(xmlPath);
        for (String id : idsShouldBePresent) {
            if (!ids.contains(id)) {
                log.error("ApiDoesElementsExist. Fail. ID (" + id + ") absent from list: " + ids + " of xmlPath (" + xmlPath + ").");
                fail("ApiDoesElementsExist. Fail. ID (" + id + ") absent from list: " + ids + " of xmlPath (" + xmlPath + ").");
            }
        }
        log.info("ApiDoesElementsExist. Passed. idsShouldBePresent (" + idsShouldBePresent + ") = IDs (" + ids + ") from xmlPath (" + xmlPath + ").");
    }
}
