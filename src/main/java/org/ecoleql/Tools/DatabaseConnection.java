package org.ecoleql.Tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class DatabaseConnection {
    private static final String JDBC_URL = "jdbc:mysql://192.168.102.71:3308/prestashop";
    private static final String USER = "presta_user";
    private static final String PASSWORD = "presta_password";
    private Logger log = LoggerFactory.getLogger(this.getClass());

    private Connection connection;

    public DatabaseConnection() {
        try {
            // Charger le pilote JDBC
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Établir la connexion à la base de données
            this.connection = DriverManager.getConnection(JDBC_URL, USER, PASSWORD);

            if (this.connection != null) {
                log.info("Connexion à la base de données établie !");
            } else {
                log.error("Échec de la connexion à la base de données.");
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            log.error("Connexion impossible");
        }
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void supprimerCommentaireParId(int commentId) {
        // La requête SQL de suppression
        String sql = "DELETE FROM ps_product_comment WHERE id_product_comment = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            // Remplir le paramètre dynamique avec l'ID du commentaire
            statement.setInt(1, commentId);

            // Exécuter la requête de suppression
            int rowsAffected = statement.executeUpdate();

            // Vérifier le nombre de lignes affectées
            if (rowsAffected > 0) {
                System.out.println("Le commentaire avec l'ID " + commentId + " a été supprimé avec succès.");
            } else {
                System.out.println("Aucune ligne n'a été supprimée.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("Attention problème dans la suppression des données en base");
        }
    }

    public boolean isCommentPresent(int productId, int customerId, String title, String content, float grade) {
        boolean isPresent = false;
        String sql = "SELECT * FROM ps_product_comment WHERE id_product = ? AND id_customer = ? AND title = ? AND content = ? AND grade = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, productId);
            statement.setInt(2, customerId);
            statement.setString(3, title);
            statement.setString(4, content);
            statement.setFloat(5, grade);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    // Un commentaire correspondant aux critères a été trouvé
                    isPresent = true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("Attention problème dans la récuparation des données en base");
        }

        return isPresent;
    }

    public boolean marquerCommentaireCommeValide(String titre, String avis) {
        try {
            // Initialiser la connexion à la base de données
            DatabaseConnection dbConnection = new DatabaseConnection();

            // Requête SQL de mise à jour pour marquer le commentaire comme validé
            String updateSql = "UPDATE ps_product_comment SET validate = 1 WHERE title = ? AND content = ?";

            try (PreparedStatement updateStatement = dbConnection.getConnection().prepareStatement(updateSql)) {
                updateStatement.setString(1, titre);
                updateStatement.setString(2, avis);

                // Exécuter la mise à jour
                int rowsAffected = updateStatement.executeUpdate();

                // Vérifier le nombre de lignes affectées
                if (rowsAffected > 0) {
                    System.out.println("Le commentaire a été marqué comme validé avec succès.");
                    return true; // Indicateur de succès
                } else {
                    System.out.println("Aucune ligne n'a été mise à jour.");
                    return false; // Aucune mise à jour
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            log.error("Attention problème dans la modification en base de données en base");
            return false; // Erreur lors de la mise à jour
        }
    }


    public int getCommentIdByTitleAndContent(int productId, int customerId, String title, String content, float grade) {
        int commentId = -1;

        // La requête SQL pour récupérer l'ID du commentaire
        String sql = "SELECT id_product_comment FROM ps_product_comment WHERE id_product = ? AND id_customer = ? AND title = ? AND content = ? AND grade = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, productId);
            statement.setInt(2, customerId);
            statement.setString(3, title);
            statement.setString(4, content);
            statement.setFloat(5, grade);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    // Récupérer l'ID du commentaire trouvé dans la base de données
                    commentId = resultSet.getInt("id_product_comment");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("Attention problème dans la récuparation des données en base");
        }

        return commentId;
    }

    public Map<String, Object> getDerniereCommandeDetails() {
        Map<String, Object> result = new HashMap<>();

        String sql = "SELECT psc.lastname AS client_nom, " +
                "psc.firstname AS client_prenom, " +
                "pa.address1 AS client_adresse, " +
                "pa.city AS client_city, " +
                "pso.total_paid AS commande_total_taxe_inclus, " +
                "pso.payment AS commande_type_paiement, " +
                "pod.product_name AS produit_nom, " +
                "pod.total_price_tax_incl AS produit_prix, " +
                "pod.product_quantity AS produit_quantite " +
                "FROM ps_orders pso " +
                "JOIN ps_customer psc ON pso.id_customer = psc.id_customer " +
                "JOIN ps_order_detail pod ON pso.id_order = pod.id_order " +
                "JOIN ps_product pp ON pod.product_id = pp.id_product " +
                "JOIN ps_address pa ON pso.id_customer = pa.id_customer " +
                "JOIN ps_customer c ON pso.id_customer = c.id_customer " + // Join avec la table ps_customer
                "WHERE pa.id_country = '8' " +
                "ORDER BY pso.id_order DESC LIMIT 1";

        try (PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            // Vérifier si le résultat contient des lignes
            if (resultSet.next()) {
                // Récupérer les métadonnées pour obtenir les noms des colonnes
                int columnCount = resultSet.getMetaData().getColumnCount();

                // Parcourir les colonnes et ajouter les valeurs à la Map
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = resultSet.getMetaData().getColumnName(i);
                    Object columnValue = resultSet.getObject(i);
                    System.out.println("colonne name: " + columnName + " colonne: " + columnValue );
                    result.put(columnName, columnValue);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("Attention problème dans la récuparation des données en base");
        }

        return result;
    }


    public void closeConnection() {
        try {
            if (this.connection != null && !this.connection.isClosed()) {
                this.connection.close();
                System.out.println("Connexion à la base de données fermée.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("Attention problème dans la récuparation des données en base");
        }
    }
}
