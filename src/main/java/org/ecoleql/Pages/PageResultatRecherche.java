package org.ecoleql.Pages;

import org.ecoleql.Tools.Outils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageResultatRecherche {

    @FindBy(xpath="//img[@alt='Mug Today is a good day']")
    private WebElement elementProduit;

    Outils outils;

    public PageProduit ClickProduit(WebDriver driver)
    {
        outils = new Outils();
        outils.clickHighLight(driver,elementProduit);
        return PageFactory.initElements(driver, PageProduit.class);
    }

    public WebElement getElementProduit() {
        return elementProduit;
    }
}
