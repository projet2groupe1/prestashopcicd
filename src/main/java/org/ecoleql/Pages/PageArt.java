package org.ecoleql.Pages;

import org.ecoleql.Tools.Outils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageArt extends PageAbstractBandeaux{

    @FindBy(xpath="//img[@alt='Affiche encadrée The best is yet to come']")
    private WebElement elementImageArticle;
    @FindBy(xpath="//p[@class='text-uppercase h6 hidden-sm-down']")
    private WebElement elementTItleFiltrerPar;

    Outils outils;

    public PageProduit ClickProduit (WebDriver driver)
    {
        outils = new Outils();
        outils.clickHighLight(driver,elementImageArticle);
        return PageFactory.initElements(driver, PageProduit.class);
    }

    // GETTERS
    public WebElement getElementImageArticle() {
        return elementImageArticle;
    }

    public WebElement getElementTItleFiltrerPar() {
        return elementTItleFiltrerPar;
    }
}
