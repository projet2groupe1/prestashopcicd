package org.ecoleql.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageProduitPromo extends PageAbstractBandeaux {

    @FindBy(xpath="//img[@alt='T-shirt imprimé colibri']")
    private WebElement elementProduitTShirtColibri;

    /* PROBLEME DE PRODUIT L'ARTICLE EN PROMO EST TAG EN PULL IMPRIME OURS BRUN DANS LE CODE HTML ALORS QUE C'EST UN PULL IMPRIME COLIBRI  */
    @FindBy(xpath="//img[@alt='Pull imprimé ours brun']")
    private WebElement elementProduitPullColibri;

    @FindBy(xpath="//a[contains(@href,'vetements') and text()='Vêtements']")
    private WebElement elementSousCategorieVetements;
    @FindBy(xpath="//a[contains(@href,'accessoires') and text()='Accessoires']")
    private WebElement elementSousCategorieAccessoires;
    @FindBy(xpath="//a[contains(@href,'art') and text()='Art']")
    private WebElement elementSousCategorieArt;

    public WebElement getElementProduitTShirtColibri() {
        return elementProduitTShirtColibri;
    }

    public WebElement getElementSousCategorieVetements() {
        return elementSousCategorieVetements;
    }

    public WebElement getElementSousCategorieAccessoires() {
        return elementSousCategorieAccessoires;
    }

    public WebElement getElementSousCategorieArt() {
        return elementSousCategorieArt;
    }

    public WebElement getElementProduitPullColibri() {
        return elementProduitPullColibri;
    }
}
