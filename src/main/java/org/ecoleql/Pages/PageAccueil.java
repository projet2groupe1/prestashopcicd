package org.ecoleql.Pages;

import org.ecoleql.Tools.Outils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageAccueil extends PageAbstractBandeaux {

    @FindBy(xpath="//h2[@class='h2 products-section-title text-uppercase' and contains(text(),'Nouveaux produits')]")
    private WebElement elementCategorieNouveauProduit;
    @FindBy(xpath="//h2[@class='h2 products-section-title text-uppercase' and contains(text(),'En promo')]")
    private WebElement elementCategorieProduitPromo;
    @FindBy(xpath="//h2[@class='h2 products-section-title text-uppercase' and contains(text(),'Nouveaux produits')]/..//li")
    private WebElement elementNouveau;
    @FindBy(xpath="//a[@class='all-product-link float-xs-left float-md-right h4' and contains(text(),'Tous les nouveaux produits')]")
    private WebElement elementTousProduitsNouveau;
    @FindBy(xpath="//a[@class='all-product-link float-xs-left float-md-right h4' and contains(text(),'Tous les produits en promotion')]")
    private WebElement elementTousProduitsPromo;


    Outils outils;
    public PageAccueil(){
        super();
    }

    @FindBy(xpath = "//h2[@class='h2 products-section-title text-uppercase']")
    protected WebElement titreArticlesPopulaires;

    @FindBy(xpath = "//article[@data-id-product-attribute='9']")
    protected WebElement articlePopulairePullColibri;

    @FindBy(xpath = "//article[@data-id-product-attribute='1']")
    protected WebElement articlePopulaireTshirtColibri;

    // Connexion au site

    public PageLogin ClickSeConnecter (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver,elementConnexion);
        return PageFactory.initElements(driver, PageLogin.class);
    }

    public  PageLogin ClickInfosPersos (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver, elementInfosPerso);
        return PageFactory.initElements(driver, PageLogin.class);
    }



    // V2
    public  PageLogin ClickConnexionBasDePage (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver, elementConnexionBasDePage);
        return PageFactory.initElements(driver, PageLogin.class);
    }

    public <T extends PageAbstractBandeaux> T ClickCategorie (WebDriver driver,String categorie)
    {
        outils = new Outils();
        if(categorie.equals("art")) {
            outils.clickHighLight(driver, elementButtonArt);
            return (T) PageFactory.initElements(driver, PageArt.class);
        }
        else
        {
            if(categorie.equals("vêtements"))
            {
                outils.clickHighLight(driver, elementButtonVetements);
                return (T) PageFactory.initElements(driver, PageVetements.class);
            }
            else
            {
                outils.clickHighLight(driver, elementButtonAccessoires);
                return (T) PageFactory.initElements(driver, PageAccessoires.class);
            }
        }
    }

    // CLique sur un produit populaire Tshirt Colibri
    public PageProduit ClickArticlePopulairePullColibri (WebDriver driver) {
        articlePopulairePullColibri.click();
        return PageFactory.initElements(driver, PageProduit.class);
    }

    // CLique sur un produit populaire Tshirt Colibri
    public PageProduit ClickArticlePopulaireTshirtColibri (WebDriver driver){
        articlePopulaireTshirtColibri.click();
        return PageFactory.initElements(driver, PageProduit.class);
    }


    public PageResultatRecherche Rechercher(WebDriver driver,String produit) {
        outils = new Outils();
        outils.sendKeysHighLight(driver,elementBarreRecherche,"Mug Today Is A Good Day");
        Actions actions = new Actions(driver);
        // Utilisez la méthode sendKeys pour envoyer la touche "Enter"
        actions.sendKeys(Keys.ENTER).build().perform();
        return PageFactory.initElements(driver, PageResultatRecherche.class);
    }

    public PageNouveuxProduit ClickTousNouveauxProduits(WebDriver driver)
    {
        outils = new Outils();
        outils.clickHighLight(driver,elementTousProduitsNouveau);
        return PageFactory.initElements(driver, PageNouveuxProduit.class);
    }

    public PageProduitPromo ClickTousProduitsPromo(WebDriver driver)
    {
        outils = new Outils();
        outils.clickHighLight(driver,elementTousProduitsPromo);
        return PageFactory.initElements(driver, PageProduitPromo.class);
    }

    public WebElement getElementCategorieNouveauProduit() {
        return elementCategorieNouveauProduit;
    }

    public WebElement getElementNouveau() {
        return elementNouveau;
    }

    public WebElement getElementtousProduitsNouveau() {
        return elementTousProduitsNouveau;
    }

    public WebElement getElementCategorieProduitPromo() {
        return elementCategorieProduitPromo;
    }
    public void SeDeconnecter (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver,elementSeDeconnecter);
    }
}
