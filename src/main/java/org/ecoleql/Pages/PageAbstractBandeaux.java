package org.ecoleql.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

abstract public class PageAbstractBandeaux {

    // Haut de page
    // Logo du site
    @FindBy(xpath="//img[@class='logo img-fluid']")
    protected WebElement elementImageMyStore;

   // Rubrique vetements
    @FindBy(xpath="//a[@class='dropdown-item' and contains(@href, 'vetements')]")
    protected WebElement elementButtonVetements;

    // Rubrique accessoire
    @FindBy(xpath="//a[@class='dropdown-item' and contains(@href, 'accessoires')]")
    protected WebElement elementButtonAccessoires;

    // Rubrique article
    @FindBy(xpath="//a[@class='dropdown-item' and contains(@href, 'art')]")
    protected WebElement elementButtonArt;

    // Bouton de login
    @FindBy(xpath="//a[@title='Identifiez-vous']")
    protected WebElement elementConnexion;

    // Barre de recherche
    @FindBy(xpath="//input[@placeholder='Rechercher']")
    protected WebElement elementBarreRecherche;

    // Lien informations personnelles en bas de page
    @FindBy(xpath="//a[@title='Informations personnelles' and contains(text(),'Informations personnelles')]")
    protected WebElement elementInfosPerso;

    // Bouton de deconnexion
    @FindBy(xpath="//a[@class='logout hidden-sm-down']")
    protected WebElement elementSeDeconnecter;
    @FindBy(xpath="//span[@class='cart-products-count']")
    protected WebElement elementNbPanier;
    @FindBy(xpath="//span[@class='hidden-sm-down' and text()='John DOE']")
    protected WebElement elementUserJohnDoe;

    // Centre Page
    @FindBy(xpath="//img[@alt='T-shirt imprimé colibri']")
    protected WebElement elementProduitColibri;


    /*
    Bas de page V1
    @FindBy(xpath="//a[@title='Informations personnelles' and contains(text(),'Informations personnelles')]")
    protected WebElement elementInfosPerso;
    */

    // Bas de page V2
    @FindBy(xpath="//a[@title='Identifiez-vous' and contains(text(),'Connexion')]")
    protected WebElement elementConnexionBasDePage;

    // GETTERS haut de page
    public WebElement getElementUserJohnDoe() {return elementUserJohnDoe;}
    public WebElement getElementNbPanier() {return elementNbPanier;}
    public WebElement getElementImageMyStore() {return elementImageMyStore;}
    public WebElement getElementButtonVetements() {return elementButtonVetements;}
    public WebElement getElementButtonAccessoires() {return elementButtonAccessoires;}
    public WebElement getElementButtonArt() {return elementButtonArt;}
    public WebElement getElementConnexion() {return elementConnexion;}
    public WebElement getElementBarreRecherche() {return elementBarreRecherche;}
    public WebElement getElementSeDeconnecter() {return elementSeDeconnecter;}

    // GETTERS centre de page
    public WebElement getElementProduitColibri() {return elementProduitColibri;}

    // GETTERS bas de page

    /* V1
    public WebElement getElementInfosPerso() {return elementInfosPerso;}
    */

    public WebElement getElementConnexionBasDePage() {
        return elementConnexionBasDePage;
    }

}
