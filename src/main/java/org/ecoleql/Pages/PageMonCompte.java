package org.ecoleql.Pages;

import org.ecoleql.Tools.Outils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageMonCompte extends PageAbstractBandeaux{
    Outils outils;
    // V2 :
    @FindBy(xpath="//a[@id='identity-link']")
    private WebElement elementBtnInformations;

    // Methodes
    public PageAccueil RetourHome (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver,elementImageMyStore);
        return PageFactory.initElements(driver, PageAccueil.class);
    }

    public PageFormulaireInfosPersos AccesFormulaireInfosPersos (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver,elementBtnInformations );
        return PageFactory.initElements(driver, PageFormulaireInfosPersos.class);
    }

    // GETTERS
    public WebElement getElementBtnMesDonnees() {return elementBtnInformations;}
}
