package org.ecoleql.Pages;

import org.ecoleql.Tools.Outils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageLogin extends PageAbstractBandeaux{

    // Champ Email
    @FindBy(id="field-email")
    private WebElement elementFieldEmail;
    // Champ Mot de passe
    @FindBy(id="field-password")
    private WebElement elementFieldMdp;

    /*
    Bouton Montrer V1
    @FindBy(xpath = "//button[@data-text-show='Montrer']")
    private WebElement elementBtnMontrer;
    //@FindBy(xpath = "//input[@id='field-password]")
    //private WebElement elementMdPMasque;
    //@FindBy(xpath = "//input[@id='field-password' and @type='text']")
    //private WebElement elementMdPAffiche;
    */

    // Bouton Afficher V2
    @FindBy(xpath = "//button[@data-text-show='Afficher']")
    private WebElement elementBtnAfficher;

    // Lien Mot de passe oublié
    @FindBy (xpath = "//a[contains(text(),'Pas de compte ? Créez-en un')]")
    private WebElement elementMdPOubli;

    // Lien Pas de compte ? Créez-en un
    @FindBy(xpath="//a[contains(text(), 'Pas de compte ? Créez-en un')]")
    private WebElement elementCreetionCompte;

    // Bouton Connexion
    @FindBy(id="submit-login")
    private WebElement elementBtnConnexion;
    @FindBy(xpath = "//li[@class='alert alert-danger']")
    private WebElement elementEchecAuthentification;
    Outils outils;


    // Methodes

    public PageMonCompte SeConnecter (WebDriver driver,String mail,String mdp) {
        outils = new Outils();
        outils.sendKeysHighLight(driver,elementFieldEmail,mail);
        outils.sendKeysHighLight(driver,elementFieldMdp,mdp);
        outils.clickHighLight(driver,elementBtnConnexion);
        return PageFactory.initElements(driver, PageMonCompte.class);
    }

    public PageAccueil Connexion (WebDriver driver,String mail,String mdp) {
        outils = new Outils();
        outils.sendKeysHighLight(driver,elementFieldEmail,mail);
        outils.sendKeysHighLight(driver,elementFieldMdp,mdp);
        outils.clickHighLight(driver,elementBtnConnexion);
        return PageFactory.initElements(driver, PageAccueil.class);
    }

    public void SaisieIdentifiants (WebDriver driver,String mail,String mdp) {
        outils = new Outils();
        outils.sendKeysHighLight(driver,elementFieldEmail,mail);
        outils.sendKeysHighLight(driver, elementFieldMdp, mdp);
    }
    /* V1
    public void MontrerMdP (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver, elementBtnMontrer);
    }
    */

    public void AfficherMdP (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver, elementBtnAfficher);
    }

    /* V1
    public PageFormulaireInfosPersos FormulairConnexion (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver,elementBtnConnexion);
        return PageFactory.initElements(driver, PageFormulaireInfosPersos.class);
    }*/

    // V2
    public PageMonCompte AccesMonCompte (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver, elementBtnConnexion);
        return  PageFactory.initElements(driver, PageMonCompte.class);
    }

    // GETTERS

    public WebElement getElementEchecAuthentification() { return elementEchecAuthentification;}
    public WebElement getElementFieldEmail() {return elementFieldEmail;}
    public WebElement getElementFieldMdp() {return elementFieldMdp;}

    /* V1
    public WebElement getElementBtnMontrer() {return elementBtnMontrer;}
    public WebElement getElementMdPMasque() {return elementMdPMasque;}
    public WebElement getElementMdPAffiche() {return elementMdPAffiche;}
    */

    public WebElement getElementBtnConnexion() {return elementBtnConnexion;}
    public WebElement getElementMdPOubli() {return elementMdPOubli;}
    public WebElement getElementCreetionCompte() {return elementCreetionCompte;}
    public WebElement getElementBtnAfficher() {return elementBtnAfficher;}
}
