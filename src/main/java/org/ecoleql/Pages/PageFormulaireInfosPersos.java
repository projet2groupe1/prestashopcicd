package org.ecoleql.Pages;

import org.ecoleql.Tools.Outils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageFormulaireInfosPersos extends PageAbstractBandeaux{
    Outils outils;

    // Element Titre Vos informations personnelles
    @FindBy(xpath="//h1[contains (text(),'Vos informations personnelles')]")
    protected WebElement elementITitre;
    // Element Civilite M
    @FindBy(id = "field-id_gender-1")
    protected WebElement elementRadioM;
    // Element Prenom
    @FindBy(id="field-firstname")
    protected WebElement elementPrenom;
    // Element Nom
    @FindBy(id="field-lastname")
    protected WebElement elementNom;
    // Element E-mail
    @FindBy(id="field-email")
    protected WebElement elementEmail;
    // Element Mot de passe
    @FindBy(id="field-password")
    protected WebElement elementMdP;
    /* V1 Element Bouton Montrer Mot de passe
    @FindBy(xpath="//input[@id='field-password']//following-sibling::span/button[@data-action='show-password']")
    protected WebElement elementMontrerMdP;
    */
    // V2 Element Bouton Afficher Mot de passe
    @FindBy(xpath="//input[@id='field-password']//following-sibling::span/button[@data-action='show-password']")
    protected WebElement elementAfficherMdP;
    // Element Nouveau Mot de passe
    @FindBy(id = "field-new_password")
    protected WebElement elementNewMdP;
    /* V1 Element Bouton Montrer Nouveau Mot de passe
    @FindBy(xpath="//input[@id='field-new_password']//following-sibling::span/button[@data-action='show-password']")
    protected WebElement elementMontrerNewMdP;
     */
    // V2 Element Bouton Afficher Nouveau Mot de passe
    @FindBy(xpath="//input[@id='field-new_password']//following-sibling::span/button[@data-action='show-password']")
    protected WebElement elementAfficherNewMdP;

    // Element Date de naissance // Assertion : input[@id='field-birthday' and contains(@value, '15/01/1970')]
    @FindBy(id = "field-birthday")
    protected WebElement elementDateNaissance;

    // Element Checkbox  'Recevoir les offres de nos partenaires'
    @FindBy(xpath="//input[@name='optin']")
    protected  WebElement elementRecevoirOffres;
    // Element Checkbox Message
    @FindBy(xpath="//input[@name='customer_privacy']")
    protected  WebElement elementMessage;
    // Element Checkbox  'Recevoir notre newsletter'
    @FindBy(xpath = "//input[@name='newsletter']")
    protected  WebElement elementRecevoirNewsletter;
    // Element Checkbox Consentement
    @FindBy(xpath="//input[@name='psgdpr']")
    protected  WebElement elementConsentement;

    // Element Enregistrer
    @FindBy(xpath="//button[@type='submit'and contains(text(), 'Enregistrer')]")
    protected WebElement elementEnregistrer;

    // Message confirmation modification
    @FindBy(xpath="//aside[@id='notifications']//article[@class='alert alert-success']/ul/li")
    protected  WebElement elementConfirmation;

    // Methodes
    public void SaisieMotsDePasse (WebDriver driver, WebElement element, String MdP) {
        outils = new Outils();
        outils.sendKeysHighLight(driver, element, MdP);
    }

    public void CliqueCheckbox (WebDriver driver, WebElement element) {
        outils = new Outils();
        element.click();
        //outils.clickHighLight(driver, element);
    }

    public void MontrerMdP (WebDriver driver, WebElement element) {
        outils = new Outils();
        outils.clickHighLight(driver, element);
    }


    // GETTERS
    public WebElement getElementITitre() {return elementITitre;}
    public WebElement getElementRadioM() {return elementRadioM;}
    public WebElement getElementPrenom() {return elementPrenom;}
    public WebElement getElementNom() {return elementNom;}
    public WebElement getElementEmail() {return elementEmail;}
    public WebElement getElementMdP() {return elementMdP;}
    public WebElement getElementNewMdP(WebDriver driver) {return driver.findElement(By.id("field-new_password"));}
    public WebElement getElementDateNaissance() {return elementDateNaissance;}
    public WebElement getElementRecevoirOffres() {return elementRecevoirOffres;}
    public WebElement getElementMessage() {return elementMessage;}
    public WebElement getElementRecevoirNewsletter() {return elementRecevoirNewsletter;}
    public WebElement getElementConsentement() {return elementConsentement;}

    /* V1
    public WebElement getElementMontrerMdP() {return elementMontrerMdP;}
    public WebElement getElementMontrerNewMdP() {return elementMontrerNewMdP;}
    */
    //V2
    public WebElement getElementAfficherMdP() {return elementAfficherMdP;}
    public WebElement getElementAfficherNewMdP() {return elementAfficherNewMdP;}

    public WebElement getElementEnregistrer() {return elementEnregistrer;}
    public WebElement getElementConfirmation() {return elementConfirmation;}
}
