package org.ecoleql.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageNouveuxProduit extends PageAbstractBandeaux{

    @FindBy(xpath="//img[contains(@src,'carnet-de-notes-ours-brun')]")
    private WebElement elementProduitOurs;
    @FindBy(xpath="//a[contains(@href,'vetements') and text()='Vêtements']")
    private WebElement elementSousCategorieVetements;
    @FindBy(xpath="//a[contains(@href,'accessoires') and text()='Accessoires']")
    private WebElement elementSousCategorieAccessoires;
    @FindBy(xpath="//a[contains(@href,'art') and text()='Art']")
    private WebElement elementSousCategorieArt;


    public WebElement getElementProduitOurs() {
        return elementProduitOurs;
    }

    public WebElement getElementSousCategorieVetements() {
        return elementSousCategorieVetements;
    }

    public WebElement getElementSousCategorieAccessoires() {
        return elementSousCategorieAccessoires;
    }

    public WebElement getElementSousCategorieArt() {
        return elementSousCategorieArt;
    }
}
