package org.ecoleql.Pages;

import org.ecoleql.Tools.Outils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PageAccessoires extends PageAbstractBandeaux{

    @FindBy(xpath="//p[@class='text-uppercase h6 hidden-sm-down']")
    private WebElement elementTItleFiltrerPar;
    @FindBy(xpath="//input[contains(@data-search-url,'Composition-Carton+recycl')]")
    private WebElement elementFiltreCartonRecycle;
    @FindBy(xpath="//img[contains(@src,'carnet-de-notes-ours-brun')]")
    private WebElement elementProduitCarnetOurs;
    @FindBy(xpath="//h2[@class='subcategory-heading' and text()='Sous-catégories']")
    private WebElement elementSousCategorie;
    @FindBy(xpath="//a[@class='text-uppercase h6' and text()='Accessoires']")
    private WebElement elementCategorie;





    Outils outils;
    WebDriverWait wait;

    public PageProduit ClickProduit (WebDriver driver,String produit)
    {
        outils = new Outils();
        if(produit.equals("Carnet De Notes Ours Brun"))
        {
            outils.clickHighLight(driver,elementProduitCarnetOurs);
            return PageFactory.initElements(driver, PageProduit.class);
        }
        return PageFactory.initElements(driver, PageProduit.class);
    }

    public int Filtrer(WebDriver driver, String typeFiltre)
    {
        outils = new Outils();
        if(typeFiltre.equals("Carton recyclé"))
        {

            elementFiltreCartonRecycle.click();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return outils.NbArticles(driver,"//div[@class='js-product product col-xs-12 col-sm-6 col-xl-4']");
    }

    // GETTERS
    public WebElement getElementFiltreCartonRecycle() {
        return elementFiltreCartonRecycle;
    }

    public WebElement getElementProduitCarnetOurs() {
        return elementProduitCarnetOurs;
    }

    public WebElement getElementSousCategorie() {
        return elementSousCategorie;
    }

    public WebElement getElementTItleFiltrerPar() {
        return elementTItleFiltrerPar;
    }

    public WebElement getElementCategorie() {
        return elementCategorie;
    }
}
