package org.ecoleql.Pages;

import org.ecoleql.Tools.DatabaseConnection;
import org.ecoleql.Tools.Outils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageProduit extends PageAbstractBandeaux{

    @FindBy(xpath="//button[@class='btn btn-touchspin js-touchspin bootstrap-touchspin-up']")
    private WebElement elementBtnQuantiteFlecheHaut;
    @FindBy(id="quantity_wanted")
    private WebElement elementFieldQuantite;
    @FindBy(xpath="//button[@class='btn btn-primary add-to-cart']")
    private WebElement elementBtnAddPanier;
    @FindBy(xpath="//button[@class='btn btn-secondary' and text()='Continuer mes achats']")
    private WebElement elementBtnContinuerAchat;
    @FindBy(id="group_3")
    private WebElement elementSelectDimension;
    @FindBy(xpath="//option[@value='20']")
    private WebElement elementSelectValue2;
    @FindBy(xpath="//option[@value='21']")
    private WebElement elementSelectValue3;
    @FindBy(xpath="//a[@class='btn btn-primary' and text()='Commander']")
    private WebElement elementBtnCommander;
    @FindBy(xpath="//h1")
    private WebElement elementTitleProduit;
    @FindBy(xpath="//span[@class='current-price-value']")
    private WebElement elementPrixProduit;
    @FindBy(xpath="//h4[@id='myModalLabel']")
    private WebElement elementTitleModal;
    @FindBy(xpath="//span[@class='shipping value']")
    private WebElement elementTransportPrix;
    @FindBy(xpath="//span[@class='value']")
    private WebElement elementPrixTTC;
    @FindBy(xpath = "//button[@class='btn btn-comment btn-comment-big post-product-comment']")
    private WebElement btnAjouterCommentaire;
    @FindBy(xpath = "//p[@class='h2' and contains (text(),'Donnez votre avis')]")
    private WebElement titreAjoutCommentaire;
    @FindBy(xpath = "//input[@id='comment_title']")
    private WebElement ajouterTitreCommentaire;
    @FindBy(xpath = "//textarea[@id='comment_content']")
    private WebElement ajouterAvisCommentaire;
    @FindBy(xpath = "//div[@class='star'][2]")
    private WebElement ajouterCinqEtoiles;
    @FindBy(xpath = "//button[@class='btn btn-comment btn-comment-big']")
    private WebElement btnValiderCommentaire;
    @FindBy(xpath = "//p[@class='h2' and contains(., 'Avis envoyé')]")
    private WebElement popUpCommentaireValide;
    @FindBy(xpath = "//div[@id='product-comment-posted-modal-message']//following-sibling::div//button")
    private WebElement btnOkCommentaire;
    @FindBy(xpath = "//p[@class='h4']")
    private WebElement verifAjoutTitreCommentaire;
    @FindBy(xpath = "//p[preceding-sibling::p[@class='h4']]")
    private WebElement verifAjoutAvisCommentaire;

    Outils outils;
    private Logger log = LoggerFactory.getLogger(this.getClass());

    public void ChoisirArticle (WebDriver driver,String value)
    {
        outils = new Outils();

        // J'ajoute au panier mon article
        if(value == null)
        {
            outils.clickHighLight(driver,elementBtnQuantiteFlecheHaut);
        }
        else
        {
            outils.clickHighLight(driver,elementSelectDimension);
            if(value.equals("2"))
            {
                outils.clickHighLight(driver,elementSelectValue2);
            }
            else
            {
                outils.clickHighLight(driver,elementSelectValue3);
            }
        }
    }

    public void AjoutPanier(WebDriver driver)
    {
        outils.clickHighLight(driver,elementBtnAddPanier);
    }
    public PageRecapProduit CliqueAjoutPanier(WebDriver driver)
    {
        outils = new Outils();
        outils.clickHighLight(driver,elementBtnAddPanier);

        // Je click sur Commander
        outils.clickHighLight(driver,elementBtnCommander);
        return PageFactory.initElements(driver, PageRecapProduit.class);
    }
    public PageRecapProduit clickCommanderOuContinuer(WebDriver driver,boolean continuer)
    {
        if(continuer)
        {
            outils.clickHighLight(driver,elementBtnContinuerAchat);
        }
        else
        {
            outils.clickHighLight(driver,elementBtnCommander);
        }
        return PageFactory.initElements(driver, PageRecapProduit.class);
    }

    //GETTERS
    public WebElement getElementBtnQuantiteFlecheHaut() {
        return elementBtnQuantiteFlecheHaut;
    }

    public WebElement getElementPrixProduit() {
        return elementPrixProduit;
    }

    public WebElement getElementFieldQuantite() {
        return elementFieldQuantite;
    }

    public WebElement getElementBtnAddPanier() {
        return elementBtnAddPanier;
    }

    public WebElement getElementBtnContinuerAchat() {
        return elementBtnContinuerAchat;
    }

    public WebElement getElementSelectDimension() {
        return elementSelectDimension;
    }

    public WebElement getElementSelectValue2() {
        return elementSelectValue2;
    }

    public WebElement getElementSelectValue3() {
        return elementSelectValue3;
    }

    public WebElement getElementBtnCommander() {
        return elementBtnCommander;
    }

    public WebElement getElementTitleProduit() {
        return elementTitleProduit;
    }

    public WebElement getElementTitleModal() {
        return elementTitleModal;
    }

    public WebElement getElementTransportPrix() {
        return elementTransportPrix;
    }

    public WebElement getElementPrixTTC() {
        return elementPrixTTC;
    }

    public PageRecapProduit CommanderUnArticle (WebDriver driver){
        outils = new Outils();
        outils.clickHighLight(driver,elementBtnAddPanier);
        outils.clickHighLight(driver,elementBtnCommander);
        return PageFactory.initElements(driver, PageRecapProduit.class);
    }

    public void AjouterCommentaire (WebDriver driver){
        outils = new Outils();
        outils.clickHighLight(driver,btnAjouterCommentaire);
    }

    public void CompleterCommentaire (WebDriver driver) {
        outils = new Outils();
        outils.clickHighLight(driver, ajouterTitreCommentaire);
        outils.sendKeysHighLight(driver, ajouterTitreCommentaire, "Ceci est un titre de commentaire");
        outils.clickHighLight(driver, ajouterAvisCommentaire);
        outils.sendKeysHighLight(driver, ajouterAvisCommentaire, "Ceci est un avis pour un commentaire");
        outils.clickHighLight(driver, ajouterCinqEtoiles);
        outils.clickHighLight(driver, btnValiderCommentaire);
    }

    // Pour verif titre commentaire
    public WebElement getTitreAjoutCommentaire(){
        return titreAjoutCommentaire;
    }

    // Pour verif titre pop up commentaire
    public WebElement getTitrePopUpCommentaire(){
        return popUpCommentaireValide;
    }

    // Verif commentaire après validate base
    public WebElement getVerifAjoutAvisCommentaire() {
        return verifAjoutAvisCommentaire;
    }
    public WebElement getVerifAjoutTitreCommentaire(){
        return verifAjoutTitreCommentaire;

    }

    // Méthode pour vérifier le commentaire dans la base de données
    public boolean verifyCommentInDatabase(int productId, int customerId, String title, String content, float grade) {
        // Initialiser la connexion à la base de données
        DatabaseConnection dbConnection = new DatabaseConnection();

        // Appeler la méthode de vérification des commentaires
        boolean isCommentPresent = dbConnection.isCommentPresent(productId, customerId, title, content, grade);

        // Afficher le résultat du test
        if (isCommentPresent) {
            System.out.println("Le commentaire est présent dans la base de données, les données sont : " + title +", "+ content + "et " + grade);

            // Récupérer l'ID du commentaire
            int idCommentaire = dbConnection.getCommentIdByTitleAndContent(productId, customerId, title, content, grade);
            log.info("L'ID du commentaire est : " + idCommentaire);
        } else {
            log.error("Le commentaire n'est pas présent dans la base de données, les données sont : \" + title +\", \"+ content + \"et \" + grade");
        }

        // Fermer la connexion à la base de données à la fin du test
        dbConnection.closeConnection();

        // Retourner le résultat de la vérification
        return isCommentPresent;
    }

    public boolean MajValidationCommentaire(WebDriver driver, String titre, String avis) {
        // Créer ou obtenir une instance de DatabaseConnection
        DatabaseConnection dbConnection = new DatabaseConnection();

        // Appeler la méthode de mise à jour dans DatabaseConnection
        return dbConnection.marquerCommentaireCommeValide(titre, avis);
    }

    public void cliqueOkCommentaire (WebDriver driver){
        outils.clickHighLight(driver, btnOkCommentaire);
    }

    public void rafraichirPage (WebDriver driver){
        // Utilisez la méthode sendKeys pour envoyer la touche "f5"
        driver.navigate().refresh();

    }

    public void suppressionDuDernierCommentaire(WebDriver driver, int productId, int customerId, String title, String content) {
        // Initialiser la connexion à la base de données
        DatabaseConnection dbConnection = new DatabaseConnection();

        // Récupérer le titre du dernier commentaire publié
        String dernierTitreCommentaire = "Ceci est un titre de commentaire";

        // Récupérer l'ID du commentaire à supprimer
        int idCommentaireASupprimer = dbConnection.getCommentIdByTitleAndContent(productId, customerId, dernierTitreCommentaire, content, 5);

        // Supprimer le commentaire en utilisant l'ID
        dbConnection.supprimerCommentaireParId(idCommentaireASupprimer);

        // Vérifier si le commentaire a été supprimé en vérifiant à nouveau sa présence
        boolean isCommentPresentAfterDeletion = dbConnection.isCommentPresent(productId, customerId, dernierTitreCommentaire, content, 5);

        // Afficher le résultat de la vérification
        if (!isCommentPresentAfterDeletion) {
            log.info("Le commentaire avec l'ID " + idCommentaireASupprimer + " a été supprimé avec succès.");
        } else {
            log.error("La suppression du commentaire a échoué ou le commentaire est toujours présent dans la base de données.");
        }

        // Fermer la connexion à la base de données à la fin de l'utilisation
        dbConnection.closeConnection();
    }


}


