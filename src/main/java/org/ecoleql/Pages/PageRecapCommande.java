package org.ecoleql.Pages;

import org.ecoleql.Tools.DatabaseConnection;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PageRecapCommande extends PageAbstractBandeaux {
    @FindBy(xpath = "//a[@class='btn btn-primary' and text()='Commander']")
    private WebElement elementBtnCommander;
    @FindBy(xpath="//h3[@class='h1 card-title']")
    private WebElement elementCommandeConfirmee;
    @FindBy(xpath="//span[text()='Affiche encadrée The best is yet to come (Dimension : 40x60cm)']/..//following-sibling::div//div[@class='col-xs-4 text-sm-center text-xs-left']")
    private WebElement elementPrixUnitaireArticle1;
    @FindBy(xpath="//span[text()='Affiche encadrée The best is yet to come (Dimension : 60x90cm)']/..//following-sibling::div//div[@class='col-xs-4 text-sm-center text-xs-left']")
    private WebElement elementPrixUnitaireArticle2;
    @FindBy(xpath="//span[text()='Affiche encadrée The best is yet to come (Dimension : 40x60cm)']/..//following-sibling::div//div[@class='col-xs-4 text-sm-center text-xs-left']//following-sibling::div[@class='col-xs-4 text-sm-center text-xs-right bold']")
    private WebElement elementPrixTotalArticle1;
    @FindBy(xpath="//span[text()='Affiche encadrée The best is yet to come (Dimension : 60x90cm)']/..//following-sibling::div//div[@class='col-xs-4 text-sm-center text-xs-left']//following-sibling::div[@class='col-xs-4 text-sm-center text-xs-right bold']")
    private WebElement elementPrixTotalArticle2;
    @FindBy(xpath="//td//span[@class='text-uppercase' and text()='Total (TTC)']/..//following-sibling::td")
    private WebElement elementPrixTotalTTC;
    private Logger log = LoggerFactory.getLogger(this.getClass());


    public void effectuerVerificationCommande() {
        DatabaseConnection dbConnection = new DatabaseConnection();
        StringBuilder errorMessage = new StringBuilder();

        boolean verificationSuccess = verifierDerniereCommandeDetails(dbConnection, errorMessage);

        if (verificationSuccess) {
            log.info("Les informations de la dernière commande sont correctes.");
        } else {
            log.error("Erreur lors de la vérification : " + errorMessage.toString());
        }

        dbConnection.closeConnection();
    }

    private boolean verifierDerniereCommandeDetails(DatabaseConnection dbConnection, StringBuilder errorMessage) {
        // Récupérer les données de la dernière commande en utilisant la méthode getDerniereCommandeDetails
        Map<String, Object> resultMap = dbConnection.getDerniereCommandeDetails();

        try {
            // Vérifier si le résultat contient des données
            log.info("On vérifie le résultat contient des données");
            if (!resultMap.isEmpty()) {
                // Définir les valeurs attendues
                Map<String, Object> expectedValues = new HashMap<>();
                expectedValues.put("lastname", "DOE");
                expectedValues.put("firstname", "John");
                expectedValues.put("address1", "16, Main street");
                expectedValues.put("city", "Paris ");
                expectedValues.put("total_paid", 22.940000f);
                expectedValues.put("payment", "Transfert bancaire");
                expectedValues.put("product_name", "T-shirt imprimé colibri (Taille : S - Couleur : Blanc)");
                expectedValues.put("total_price_tax_incl", 22.940000f);
                expectedValues.put("product_quantity", "1");

                // Comparer les valeurs
                log.info("On compare les valeurs récupérées en base à l'attendu");
                for (Map.Entry<String, Object> entry : resultMap.entrySet()) {
                    String key = entry.getKey();
                    Object actualValue = entry.getValue();
                    Object expectedValue = expectedValues.get(key);
                    if (expectedValue instanceof Float){
                        String expect = String.format("%.2f", expectedValue);
                        String actual = String.format("%.2f", actualValue);
                      assert expect.equals(actual);
                    } else {
                        assert actualValue != null;
                        assert Objects.equals(actualValue.toString(), expectedValue) :
                                "La valeur de '" + key + "' ne correspond pas. Attendu : " + expectedValue + ", Actuel : " + actualValue;
                    }

                }

            } else {
                errorMessage.append("Aucune commande trouvée.");
                log.error("Attention problème dans la récuparation de la commande en base");
                throw new AssertionError("Le test échoue car aucune commande n'a été trouvée.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            errorMessage.append("Erreur lors de la récupération des données de commande.");
            log.error("Attention problème dans la récuparation des données en base");
            return false;
        }

        return true;
    }

    public WebElement getElementCommandeConfirmee() {
        return elementCommandeConfirmee;
    }

    public WebElement getElementPrixUnitaireArticle1() {
        return elementPrixUnitaireArticle1;
    }

    public WebElement getElementPrixUnitaireArticle2() {
        return elementPrixUnitaireArticle2;
    }

    public WebElement getElementPrixTotalArticle1() {
        return elementPrixTotalArticle1;
    }

    public WebElement getElementPrixTotalArticle2() {
        return elementPrixTotalArticle2;
    }

    public WebElement getElementPrixTotalTTC() {
        return elementPrixTotalTTC;
    }
}




