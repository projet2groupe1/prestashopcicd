package org.ecoleql.Pages;

import org.ecoleql.Tools.Outils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageRecapProduit extends PageAbstractBandeaux {

    @FindBy(xpath="//a[@class='btn btn-primary' and text()='Commander']")
    private WebElement elementBtnCommander;
    @FindBy(xpath="//a[@data-id-product-attribute='15']//i[@class='material-icons float-xs-left']")
    private WebElement elementBtnSupprimer;
    @FindBy(xpath="//span[@class='value' and text()='80x120cm']")
    private WebElement elementProduit80x120;
    @FindBy(xpath="//div[@class='cart-summary-line cart-total']//span[@class='value']")
    private WebElement elementTotalPanier;



    Outils outils;
    public void Supprimer (WebDriver driver)
    {
        outils = new Outils();
        outils.clickHighLight(driver,elementBtnSupprimer);
    }
    public PageInfosPersos Commander (WebDriver driver)
    {
        elementBtnCommander.click();
        return PageFactory.initElements(driver, PageInfosPersos.class);
    }

    public WebElement getElementProduit80x120() {
        return elementProduit80x120;
    }

    public WebElement getElementTotalPanier() {
        return elementTotalPanier;
    }

}
