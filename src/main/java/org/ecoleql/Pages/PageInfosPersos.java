package org.ecoleql.Pages;

import org.ecoleql.Tools.Outils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PageInfosPersos extends PageAbstractBandeaux{

    @FindBy(xpath="//button[@class='btn btn-primary continue float-xs-right' and contains(text(), 'Continuer')]")
    private WebElement elementBtnContinuerAddress;
    @FindBy(xpath="//button[@name='confirmDeliveryOption' and contains(text(), 'Continuer')]")
    private WebElement elementBtnContinuerLivraison;
    @FindBy(xpath="//div[@id='payment-option-2-container']//span[@class='custom-radio float-xs-left']")
    private WebElement elementCheckboxPaiementCheque;
    @FindBy(xpath = "//div[@id='payment-option-3-container']//span[@class='custom-radio float-xs-left']")
    private WebElement elementCheckboxPaiementCarte;
    @FindBy(xpath="//div[@class='float-xs-left']//span[@class='custom-checkbox']//input")
    private WebElement elementCheckboxCondition;
    //@FindBy(xpath="//input[contains(@id,'conditions_to_approve')]")
    @FindBy(xpath="//button[@class='btn btn-primary center-block' and contains(text(), 'Commander')]")
    private WebElement elementBtnCommander;
    @FindBy(xpath="//section[@id='checkout-personal-information-step']")
    private WebElement elementSectionInfoPerso;
    @FindBy(xpath="//span[@class='h6 carrier-name' and text()='Click and collect']")
    private WebElement elementClickAndCollect;
    @FindBy(xpath="//span[@class='h6 carrier-name' and text()='My carrier']")
    private WebElement elementMyCarrier;



    Outils outils;

    public void ClickContinuerAdresse (WebDriver driver)
    {
        outils = new Outils();
        outils.clickHighLight(driver,elementBtnContinuerAddress);
    }

    public void ClickContinuerLivraison (WebDriver driver)
    {
        outils = new Outils();
        outils.clickHighLight(driver,elementBtnContinuerLivraison);
    }
    public PageRecapCommande Paiement (WebDriver driver)
    {
        outils.clickHighLight(driver,elementCheckboxPaiementCarte);
        new WebDriverWait(driver, Duration.ofSeconds(3)).until(d -> {
            return elementCheckboxCondition.isEnabled();

        });
        elementCheckboxCondition.click();
        outils.clickHighLight(driver,elementBtnCommander);
        return PageFactory.initElements(driver, PageRecapCommande.class);

    }

    public WebElement getElementSectionInfoPerso() {
        return elementSectionInfoPerso;
    }

    public WebElement getElementBtnContinuerAddress() {
        return elementBtnContinuerAddress;
    }

    public WebElement getElementClickAndCollect() {
        return elementClickAndCollect;
    }

    public WebElement getElementMyCarrier() {
        return elementMyCarrier;
    }

    public WebElement getElementCheckboxPaiementCarte() {
        return elementCheckboxPaiementCarte;
    }




}
